package org.gcube.portlets.user.dataminermanagertester.shared.result;

import java.io.Serializable;

import org.gcube.data.analysis.dataminermanagercl.shared.process.ComputationStatus;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class SingleTestResult implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4779062510507675602L;
	private ComputationStatus computationStatus;
	private String response;

	public SingleTestResult() {
		super();
	}

	public SingleTestResult(ComputationStatus computationStatus, String response) {
		super();
		this.computationStatus = computationStatus;
		this.response = response;
	}

	public ComputationStatus getComputationStatus() {
		return computationStatus;
	}

	public void setComputationStatus(ComputationStatus computationStatus) {
		this.computationStatus = computationStatus;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	@Override
	public String toString() {
		return "SingleTestResult [computationStatus=" + computationStatus + ", response=" + response + "]";
	}

}
