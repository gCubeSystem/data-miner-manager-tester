package org.gcube.portlets.user.dataminermanagertester.server;

import java.util.HashMap;
import java.util.concurrent.Callable;

import javax.servlet.http.HttpSession;

import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.portlets.user.dataminermanagertester.server.task.TaskRequest;
import org.gcube.portlets.user.dataminermanagertester.server.task.TaskWrapper;
import org.gcube.portlets.user.dataminermanagertester.server.testbatch.TesterCaller;
import org.gcube.portlets.user.dataminermanagertester.shared.exception.ServiceException;
import org.gcube.portlets.user.dataminermanagertester.shared.result.BatchTestResult;
import org.gcube.portlets.user.dataminermanagertester.shared.task.TaskStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
public class DataMinerTesterCallable implements Callable<TaskStatus> {

	private static Logger logger = LoggerFactory.getLogger(DataMinerTesterCallable.class);

	private TaskRequest taskRequest;

	public DataMinerTesterCallable(TaskRequest taskRequest) {
		super();
		this.taskRequest = taskRequest;
		logger.debug("DataMinerTesterCallable: " + taskRequest);
	}

	@Override
	public TaskStatus call() throws Exception {
		try {

			HttpSession httpSession = taskRequest.getHttpSession();
			if (httpSession == null) {
				logger.error("Error retrieving HttpSession in DataMinerTesterCallable: is null");
				return TaskStatus.ERROR;
			}

			logger.debug("Set SecurityToken: " + taskRequest.getServiceCredentials().getToken());
			SecurityTokenProvider.instance.set(taskRequest.getServiceCredentials().getToken());
			logger.debug("Set ScopeProvider: " + taskRequest.getServiceCredentials().getScope());
			ScopeProvider.instance.set(taskRequest.getServiceCredentials().getScope());
			TesterCaller testerCaller = new TesterCaller();

			BatchTestResult result = null;

			try {
				result = testerCaller.runTest(taskRequest);

			} catch (ServiceException e) {

				TaskWrapper taskWrapper = new TaskWrapper(taskRequest.getOperationId(), TaskStatus.ERROR,
						e.getLocalizedMessage());

				HashMap<String, TaskWrapper> taskWrapperMap = SessionUtil.getTaskWrapperMap(httpSession,
						taskRequest.getServiceCredentials());
				if (taskWrapperMap == null) {
					taskWrapperMap = new HashMap<>();
					SessionUtil.setTaskWrapperMap(httpSession, taskRequest.getServiceCredentials(), taskWrapperMap);

				}

				taskWrapperMap.put(taskWrapper.getOperationId(), taskWrapper);

				return TaskStatus.ERROR;
			}


			TaskWrapper taskWrapper = new TaskWrapper(taskRequest.getOperationId(), TaskStatus.COMPLETED,
					result);

			HashMap<String, TaskWrapper> taskWrapperMap = SessionUtil.getTaskWrapperMap(httpSession,
					taskRequest.getServiceCredentials());

			if (taskWrapperMap == null) {
				taskWrapperMap = new HashMap<>();
				SessionUtil.setTaskWrapperMap(httpSession, taskRequest.getServiceCredentials(), taskWrapperMap);

			}

			taskWrapperMap.put(taskWrapper.getOperationId(), taskWrapper);
			return TaskStatus.COMPLETED;

		} catch (Throwable e) {
			logger.error("DataMinerTesterDaemon Execute(): " + e.getLocalizedMessage(), e);
			return TaskStatus.ERROR;
		}

	}
}