package org.gcube.portlets.user.dataminermanagertester.client.place;


/**
 * 
 * @author Giancarlo Panichi
 *
 */
public class NameTokens {
	public static final String HOME = "home";
	public static final String HELP = "help";
	public static final String TEST = "test";
	public static final String BATCH = "batch";
	public static final String DIFF = "diff";
	
	public static String getHome() {
		return HOME;
	}

	public static String getHelp() {
		return HELP;
	}

	public static String getTest() {
		return TEST;
	}
	
	public static String getDiff() {
		return DIFF;
	}
	
	public static String getBatch() {
		return BATCH;
	}
	
}
