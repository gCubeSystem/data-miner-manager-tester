package org.gcube.portlets.user.dataminermanagertester.client.application.testbatchconfig;

import com.gwtplatform.mvp.client.gin.AbstractPresenterModule;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
public class TestBatchConfModule extends AbstractPresenterModule {
    @Override
    protected void configure() {
        bindPresenter(TestBatchConfPresenter.class, TestBatchConfPresenter.PresenterView.class, 
        		TestBatchConfView.class, TestBatchConfPresenter.PresenterProxy.class);
    }
}