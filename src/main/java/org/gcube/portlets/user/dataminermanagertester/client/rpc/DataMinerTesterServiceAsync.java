package org.gcube.portlets.user.dataminermanagertester.client.rpc;

import org.gcube.data.analysis.dataminermanagercl.shared.data.computations.ComputationId;
import org.gcube.data.analysis.dataminermanagercl.shared.process.ComputationStatus.Status;
import org.gcube.portlets.user.dataminermanagertester.shared.config.DMBatchConfig;
import org.gcube.portlets.user.dataminermanagertester.shared.config.DMConfig;
import org.gcube.portlets.user.dataminermanagertester.shared.config.DMDiffConfig;
import org.gcube.portlets.user.dataminermanagertester.shared.result.BatchTestResult;
import org.gcube.portlets.user.dataminermanagertester.shared.session.UserInfo;
import org.gcube.portlets.user.dataminermanagertester.shared.task.TaskStatus;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public interface DataMinerTesterServiceAsync {

	// public static DataMinerTesterServiceAsync INSTANCE =
	// (DataMinerTesterServiceAsync) GWT
	// .create(DataMinerTesterService.class);

	void hello(String token, AsyncCallback<UserInfo> callback);

	void getCapabilities(String token, DMConfig dmConfig, AsyncCallback<String> callback);

	void getOperatorsDiff(String token, DMDiffConfig operatorsDiffConfig, AsyncCallback<String> callback);

	void startSimpleTest(String token, DMConfig dmConfig, AsyncCallback<ComputationId> callback);

	void retrieveOutputForSimpleTest(String token, DMConfig dmConfig, ComputationId computationId,
			AsyncCallback<String> callback);

	void monitorSimpleTest(String token, DMConfig dmConfig, ComputationId computationId,
			AsyncCallback<Status> callback);

	void startBatchTest(String token, DMBatchConfig dmBatchConfig, AsyncCallback<String> callback);

	void monitorBatchTest(String token, String operationId, AsyncCallback<TaskStatus> callback);

	void retrieveOutputForBatchTest(String token, String operationId, AsyncCallback<BatchTestResult> callback);

}
