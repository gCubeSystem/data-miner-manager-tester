package org.gcube.portlets.user.dataminermanagertester.server.testconfig;

import java.util.ArrayList;
import java.util.List;

import org.gcube.data.analysis.dataminermanagercl.shared.data.OutputData;
import org.gcube.data.analysis.dataminermanagercl.shared.data.output.MapResource;
import org.gcube.data.analysis.dataminermanagercl.shared.data.output.Resource;
import org.gcube.data.analysis.dataminermanagercl.shared.parameters.ColumnListParameter;
import org.gcube.data.analysis.dataminermanagercl.shared.parameters.ColumnParameter;
import org.gcube.data.analysis.dataminermanagercl.shared.parameters.ListParameter;
import org.gcube.data.analysis.dataminermanagercl.shared.parameters.ObjectParameter;
import org.gcube.data.analysis.dataminermanagercl.shared.parameters.Parameter;
import org.gcube.data.analysis.dataminermanagercl.shared.parameters.TabularParameter;
import org.gcube.data.analysis.dataminermanagercl.shared.process.Operator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class FeedForwardAnnTest implements DMTest {
	private static Logger logger = LoggerFactory.getLogger(FeedForwardAnnTest.class);
	private static final String id = "org.gcube.dataanalysis.wps.statisticalmanager.synchserver.mappedclasses.modellers.FEED_FORWARD_ANN";

	@Override
	public String getId() {
		return id;
	}

	@Override
	public void createRequest(Operator operator) {
		TabularParameter trainingDataSet = new TabularParameter();
		trainingDataSet.setName("TrainingDataSet");
		trainingDataSet.setValue("https://data.d4science.org/K0ZoUlVDNW1hR0hDZWZucS9UQkJmUVRTeWRuRVBGUC9HbWJQNStIS0N6Yz0");
		
		ColumnListParameter trainingColumns = new ColumnListParameter();
		trainingColumns.setName("TrainingColumns");
		trainingColumns.setValue("depthmin|depthmax|depthmean|depthsd");
			
		ColumnParameter targetColumn = new ColumnParameter();
		targetColumn.setName("TargetColumn");
		targetColumn.setValue("sstanmean");
		
		ListParameter layersNeurons = new ListParameter();
		layersNeurons.setName("LayersNeurons");
		layersNeurons.setValue("20");
			
		ObjectParameter reference = new ObjectParameter();
		reference.setName("Reference");
		reference.setValue("1");
			
		ObjectParameter learningThreshold = new ObjectParameter();
		learningThreshold.setName("LearningThreshold");
		learningThreshold.setValue("0.0001");
			
		ObjectParameter maxIterations = new ObjectParameter();
		maxIterations.setName("MaxIterations");
		maxIterations.setValue("10");
			
		ObjectParameter modelName = new ObjectParameter();
		modelName.setName("ModelName");
		modelName.setValue("trained_network");
			
		List<Parameter> parameters = new ArrayList<>();
		parameters.add(trainingDataSet);
		parameters.add(trainingColumns);
		parameters.add(targetColumn);
		parameters.add(layersNeurons);
		parameters.add(reference);
		parameters.add(learningThreshold);
		parameters.add(maxIterations);
		parameters.add(modelName);
			
		operator.setOperatorParameters(parameters);

	}

	@Override
	public String getResult(OutputData outputData) {
		StringBuilder result=new StringBuilder();
		logger.debug("Output: " + outputData);
		Resource resource = outputData.getResource();
		if (resource.isMap()) {
			MapResource mapResource = (MapResource) resource;
			for (String key : mapResource.getMap().keySet()) {
				logger.debug("Entry: " + key + " = "
						+ mapResource.getMap().get(key));
				result.append("Entry: " + key + " = "
						+ mapResource.getMap().get(key));
			}
			
		} else {
			
		}
		return result.toString();
	}

	@Override
	public boolean isValidResult(OutputData outputData) {
		boolean valid;
		logger.debug("Output: " + outputData);
		Resource resource = outputData.getResource();
		if (resource.isMap()) {
			MapResource mapResource = (MapResource) resource;
			for (String key : mapResource.getMap().keySet()) {
				logger.debug("Entry: " + key + " = "
						+ mapResource.getMap().get(key));
			}
			valid=true;
			
		} else {
			valid=false;
		}
		return valid;
	
	}
	
}
