package org.gcube.portlets.user.dataminermanagertester.client.application.testconfig;

import javax.inject.Inject;

import org.gcube.portlets.user.dataminermanagertester.shared.config.DMConfig;
import org.gcube.portlets.user.dataminermanagertester.shared.config.ProtocolType;
import org.gcube.portlets.user.dataminermanagertester.shared.config.TestType;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Widget;
import com.gwtplatform.mvp.client.ViewWithUiHandlers;

import gwt.material.design.client.constants.Color;
import gwt.material.design.client.constants.IconType;
import gwt.material.design.client.ui.MaterialButton;
import gwt.material.design.client.ui.MaterialListBox;
import gwt.material.design.client.ui.MaterialTextArea;
import gwt.material.design.client.ui.MaterialTextBox;
import gwt.material.design.client.ui.animate.MaterialAnimation;
import gwt.material.design.client.ui.animate.Transition;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
class TestConfView extends ViewWithUiHandlers<TestConfUiHandlers> implements TestConfPresenter.PresenterView {
	interface Binder extends UiBinder<Widget, TestConfView> {
	}

	@UiField
	MaterialTextBox dm;

	@UiField
	MaterialTextBox token;

	@UiField
	MaterialListBox protocolType;

	@UiField
	MaterialListBox testType;

	@UiField
	MaterialTextArea result;

	@UiField
	MaterialButton resultIcon;

	@UiField
	MaterialButton run;

	@Inject
	TestConfView(Binder uiBinder) {
		initWidget(uiBinder.createAndBindUi(this));
		testType.clear();
		for (TestType tType : TestType.values()) {
			testType.add(tType.getLabel());
		}
		protocolType.clear();
		for (ProtocolType tType : ProtocolType.values()) {
			protocolType.add(tType.getLabel());
		}
		dm.setAllowBlank(false);
		token.setAllowBlank(false);
	}

	@UiHandler("run")
	void onClick(ClickEvent e) {
		if (!dm.validate() || !token.validate()) {
			return;
		}

		String dmValue = dm.getValue();
		String tokenValue = token.getValue();
		String protocolValue = protocolType.getValue();
		String testTypeValue = testType.getValue();

		resultIcon.setVisible(false);
		result.setVisible(false);
		
		DMConfig dmConfig = new DMConfig(dmValue, tokenValue, protocolValue,testTypeValue);
		getUiHandlers().executeTest(dmConfig);
	}

	@Override
	public void setResult(String resultValue, boolean success) {
		resultIcon.setVisible(true);
		result.setVisible(true);
		if (success) {
			resultIcon.setIconType(IconType.CHECK_CIRCLE);
			resultIcon.setBackgroundColor(Color.GREEN);
			resultIcon.setIconFontSize(4.0, Unit.EM);
		} else {
			resultIcon.setIconType(IconType.ERROR);
			resultIcon.setBackgroundColor(Color.RED);
			resultIcon.setIconFontSize(4.0, Unit.EM);
		}
		MaterialAnimation animation = new MaterialAnimation();
		animation.setDelay(0);
		animation.setDuration(1000);
		animation.transition(Transition.FLIPINX);
		animation.animate(resultIcon);

		result.setLength(resultValue.length());
		result.setValue(resultValue);
		// result.reinitialize();

	}

}