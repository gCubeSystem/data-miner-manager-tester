package org.gcube.portlets.user.dataminermanagertester.client.application.testbatchconfig;

import java.util.ArrayList;
import java.util.Arrays;

import javax.inject.Inject;

import org.gcube.data.analysis.dataminermanagercl.shared.process.ComputationStatus.Status;
import org.gcube.portlets.user.dataminermanagertester.client.application.custom.SelectableTextCell;
import org.gcube.portlets.user.dataminermanagertester.shared.config.DMBatchConfig;
import org.gcube.portlets.user.dataminermanagertester.shared.config.ProtocolType;
import org.gcube.portlets.user.dataminermanagertester.shared.config.TestType;
import org.gcube.portlets.user.dataminermanagertester.shared.result.BatchTestRow;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;
import com.gwtplatform.mvp.client.ViewWithUiHandlers;

import gwt.material.design.client.base.constants.StyleName;
import gwt.material.design.client.base.density.DisplayDensity;
import gwt.material.design.client.constants.Color;
import gwt.material.design.client.constants.IconType;
import gwt.material.design.client.constants.TextAlign;
import gwt.material.design.client.data.SelectionType;
import gwt.material.design.client.ui.MaterialButton;
import gwt.material.design.client.ui.MaterialIcon;
import gwt.material.design.client.ui.MaterialListBox;
import gwt.material.design.client.ui.MaterialPanel;
import gwt.material.design.client.ui.MaterialTextArea;
import gwt.material.design.client.ui.MaterialTextBox;
import gwt.material.design.client.ui.animate.MaterialAnimation;
import gwt.material.design.client.ui.animate.Transition;
import gwt.material.design.client.ui.table.MaterialDataTable;
import gwt.material.design.client.ui.table.cell.TextColumn;
import gwt.material.design.client.ui.table.cell.WidgetColumn;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
class TestBatchConfView extends ViewWithUiHandlers<TestBatchConfUiHandlers>
		implements TestBatchConfPresenter.PresenterView {
	interface Binder extends UiBinder<Widget, TestBatchConfView> {
	}

	@UiField
	MaterialTextArea dms;

	@UiField
	MaterialTextBox token;

	@UiField
	MaterialListBox protocolType;
	
	@UiField
	MaterialListBox testType;

	@UiField
	MaterialTextArea result;

	@UiField
	MaterialDataTable<BatchTestRow> resultsTable;

	@UiField
	MaterialButton resultIcon;

	@UiField
	MaterialButton run;

	@Inject
	TestBatchConfView(Binder uiBinder) {
		initWidget(uiBinder.createAndBindUi(this));
		testType.clear();
		for (TestType tType : TestType.values()) {
			testType.add(tType.getLabel());
		}
		protocolType.clear();
		for (ProtocolType tType : ProtocolType.values()) {
			protocolType.add(tType.getLabel());
		}
		dms.setAllowBlank(false);
		token.setAllowBlank(false);
	}

	@UiHandler("run")
	void onClick(ClickEvent e) {
		if (!dms.validate() || !token.validate()) {
			return;
		}

		String dmsValue = dms.getValue();
		String tokenValue = token.getValue();
		String protocolValue= protocolType.getValue();
		String testTypeValue = testType.getValue();

		resultIcon.setVisible(false);
		result.setVisible(false);
		resultsTable.setVisible(false);

		ArrayList<String> dmList;
		if (dmsValue.contains(",")) {
			String[] urlsArray = dmsValue.split(",");
			dmList = new ArrayList<>(Arrays.asList(urlsArray));
			dmList.removeAll(Arrays.asList("", null));
			
		} else {
			dmList = new ArrayList<>();
			dmList.add(dmsValue);
		}

		DMBatchConfig dmBatchConfig = new DMBatchConfig(dmList, tokenValue, protocolValue, testTypeValue);
		getUiHandlers().executeBatchTest(dmBatchConfig);
	}

	@Override
	public void setResult(String resultValue, boolean success) {
		resultIcon.setVisible(true);
		result.setVisible(true);
		if (success) {
			resultIcon.setIconType(IconType.CHECK_CIRCLE);
			resultIcon.setBackgroundColor(Color.GREEN);
			resultIcon.setIconFontSize(4.0, Unit.EM);
		} else {
			resultIcon.setIconType(IconType.ERROR);
			resultIcon.setBackgroundColor(Color.RED);
			resultIcon.setIconFontSize(4.0, Unit.EM);
		}
		MaterialAnimation animation = new MaterialAnimation();
		animation.setDelay(0);
		animation.setDuration(1000);
		animation.transition(Transition.FLIPINX);
		animation.animate(resultIcon);

		result.setLength(resultValue.length());
		result.setValue(resultValue);
		// result.reinitialize();

	}

	@Override
	public void setupTable() {
		
		TextColumn<BatchTestRow> dataminerColumn = new TextColumn<BatchTestRow>(new SelectableTextCell()) {
			@Override
			public String getValue(BatchTestRow batchTestRow) {
				return batchTestRow.getUrl();
			}

			@Override
			public boolean sortable() {
				return true;
			}
		};
		dataminerColumn.textAlign(TextAlign.LEFT);
		dataminerColumn.styleProperty(StyleName.WHITE_SPACE, "normal");

		WidgetColumn<BatchTestRow, MaterialPanel> statusColumn = new WidgetColumn<BatchTestRow, MaterialPanel>() {
			@Override
			public MaterialPanel getValue(BatchTestRow batchTestRow) {
				MaterialPanel panel = new MaterialPanel();
				panel.setStyle("unselectable='false'");
				MaterialIcon icon = null;
				Status status = batchTestRow.getStatus();
				if (status == null) {
					icon = new MaterialIcon(IconType.ERROR, Color.RED, Color.WHITE);
				} else {
					switch (status) {
					case COMPLETE:
						icon = new MaterialIcon(IconType.CHECK_CIRCLE, Color.GREEN, Color.WHITE);

						break;
					case FAILED:
						icon = new MaterialIcon(IconType.ERROR, Color.RED, Color.WHITE);
						break;
					case ACCEPTED:
					case CANCELLED:
					case RUNNING:
					default:
						icon = new MaterialIcon(IconType.REPORT_PROBLEM, Color.YELLOW, Color.WHITE);
						break;
					}
				}
				panel.add(icon);
				return panel;
			}

			@Override
			public boolean sortable() {
				return true;
			}
		};
		statusColumn.textAlign(TextAlign.CENTER);

		WidgetColumn<BatchTestRow, MaterialPanel> responseColumn = new WidgetColumn<BatchTestRow, MaterialPanel>() {
			@Override
			public MaterialPanel getValue(BatchTestRow batchTestRow) {
				MaterialPanel panel = new MaterialPanel();
				SafeHtmlBuilder sb = new SafeHtmlBuilder();
				sb.appendHtmlConstant("<div style='white-space:normal' unselectable='false'>");
				sb.appendEscaped(batchTestRow.getResponse());
				sb.appendHtmlConstant("</div>");
				HTMLPanel hpanel = new HTMLPanel(sb.toSafeHtml());
				panel.add(hpanel);
				return panel;
			}

			@Override
			public boolean sortable() {
				return true;
			}
		};
		responseColumn.textAlign(TextAlign.LEFT);

		resultsTable.addColumn("DataMiner", dataminerColumn);
		resultsTable.addColumn("Status", statusColumn);
		resultsTable.addColumn("Response", responseColumn);

	}

	@Override
	public void setResultTable(ArrayList<BatchTestRow> batchTestRows) {
		resultIcon.setVisible(true);
		resultsTable.setVisible(true);
		boolean success = true;

		for (BatchTestRow batchTestRow : batchTestRows) {
			if (batchTestRow.getStatus()==null||batchTestRow.getStatus().compareTo(Status.COMPLETE) != 0) {
				success = false;
				break;
			}
		}
		
		if (success) {
			resultIcon.setIconType(IconType.CHECK_CIRCLE);
			resultIcon.setBackgroundColor(Color.GREEN);
			resultIcon.setIconFontSize(4.0, Unit.EM);
		} else {
			resultIcon.setIconType(IconType.ERROR);
			resultIcon.setBackgroundColor(Color.RED);
			resultIcon.setIconFontSize(4.0, Unit.EM);
		}

		MaterialAnimation animation = new MaterialAnimation();
		animation.setDelay(0);
		animation.setDuration(1000);
		animation.transition(Transition.FLIPINX);
		animation.animate(resultIcon);

		resultsTable.clearRows(true);
		resultsTable.getTableTitle().setText("Results");
		resultsTable.setRowData(0, batchTestRows);
		resultsTable.getView().refresh();

		reload();
	}

	@Override
	public void setupOptions() {

		// Selection Type
		resultsTable.setSelectionType(SelectionType.NONE);
	
		// Density
		resultsTable.setDensity(DisplayDensity.DEFAULT);

	}

	public void reload() {
		resultsTable.getView().setRedraw(true);
		resultsTable.getView().refresh();
	}

}