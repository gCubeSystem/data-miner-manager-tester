package org.gcube.portlets.user.dataminermanagertester.client.application.diff;

import javax.inject.Inject;

import org.gcube.portlets.user.dataminermanagertester.shared.config.DMDiffConfig;
import org.gcube.portlets.user.dataminermanagertester.shared.config.ProtocolType;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Widget;
import com.gwtplatform.mvp.client.ViewWithUiHandlers;

import gwt.material.design.client.constants.Color;
import gwt.material.design.client.constants.IconType;
import gwt.material.design.client.ui.MaterialButton;
import gwt.material.design.client.ui.MaterialListBox;
import gwt.material.design.client.ui.MaterialTextArea;
import gwt.material.design.client.ui.MaterialTextBox;
import gwt.material.design.client.ui.animate.MaterialAnimation;
import gwt.material.design.client.ui.animate.Transition;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
class OperatorsDiffView extends ViewWithUiHandlers<OperatorsDiffUiHandlers>
		implements OperatorsDiffPresenter.PresenterView {
	interface Binder extends UiBinder<Widget, OperatorsDiffView> {
	}

	@UiField
	MaterialTextBox dm1;

	@UiField
	MaterialTextBox token1;

	@UiField
	MaterialTextBox dm2;

	@UiField
	MaterialTextBox token2;
	
	@UiField
	MaterialListBox protocolType;

	@UiField
	MaterialTextArea result;

	@UiField
	MaterialButton resultIcon;

	@UiField
	MaterialButton run;

	@Inject
	OperatorsDiffView(Binder uiBinder) {
		initWidget(uiBinder.createAndBindUi(this));
		protocolType.clear();
		for (ProtocolType tType : ProtocolType.values()) {
			protocolType.add(tType.getLabel());
		}
		
		dm1.setAllowBlank(false);
		token1.setAllowBlank(false);
		dm2.setAllowBlank(false);
		token2.setAllowBlank(false);
	}

	@UiHandler("run")
	void onClick(ClickEvent e) {
		if (!dm1.validate() || !token1.validate() || !dm2.validate() || !token2.validate()) {
			return;
		}

		String dm1Value = dm1.getValue();
		String token1Value = token1.getValue();
		String dm2Value = dm2.getValue();
		String token2Value = token2.getValue();
		String protocolValue = protocolType.getValue();

		resultIcon.setVisible(false);
		result.setVisible(false);
		
		
		DMDiffConfig operatorsDiffConfig = new DMDiffConfig(dm1Value, token1Value, dm2Value,
				token2Value, protocolValue);
		getUiHandlers().executeDiff(operatorsDiffConfig);
	}

	@Override
	public void setResult(String resultValue, boolean success) {
		resultIcon.setVisible(true);
		result.setVisible(true);
		if (success) {
			resultIcon.setIconType(IconType.CHECK_CIRCLE);
			resultIcon.setBackgroundColor(Color.GREEN);
			resultIcon.setIconFontSize(4.0, Unit.EM);
		} else {
			resultIcon.setIconType(IconType.ERROR);
			resultIcon.setBackgroundColor(Color.RED);
			resultIcon.setIconFontSize(4.0, Unit.EM);
		}
		MaterialAnimation animation = new MaterialAnimation();
		animation.setDelay(0);
		animation.setDuration(1000);
		animation.transition(Transition.FLIPINX);
		animation.animate(resultIcon);

		result.setLength(resultValue.length());
		result.setValue(resultValue);

	}

}