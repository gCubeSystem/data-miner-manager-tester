package org.gcube.portlets.user.dataminermanagertester.client.application.testbatchconfig;


import org.gcube.portlets.user.dataminermanagertester.shared.config.DMBatchConfig;

import com.gwtplatform.mvp.client.UiHandlers;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
interface TestBatchConfUiHandlers extends UiHandlers {

	void executeBatchTest(DMBatchConfig dmBatchConfig);
}
