package org.gcube.portlets.user.dataminermanagertester.client.application.diff;

import org.gcube.portlets.user.dataminermanagertester.client.application.ApplicationPresenter;
import org.gcube.portlets.user.dataminermanagertester.client.place.NameTokens;
import org.gcube.portlets.user.dataminermanagertester.client.rpc.DataMinerTesterServiceAsync;
import org.gcube.portlets.user.dataminermanagertester.shared.Constants;
import org.gcube.portlets.user.dataminermanagertester.shared.config.DMDiffConfig;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.mvp.client.HasUiHandlers;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.NoGatekeeper;
import com.gwtplatform.mvp.client.annotations.ProxyStandard;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;

import gwt.material.design.client.ui.MaterialLoader;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
public class OperatorsDiffPresenter
		extends Presenter<OperatorsDiffPresenter.PresenterView, OperatorsDiffPresenter.PresenterProxy>
		implements OperatorsDiffUiHandlers {
	interface PresenterView extends View, HasUiHandlers<OperatorsDiffUiHandlers> {
		void setResult(String result, boolean success);

	}

	@ProxyStandard
	@NameToken(NameTokens.DIFF)
	@NoGatekeeper
	interface PresenterProxy extends ProxyPlace<OperatorsDiffPresenter> {
	}

	private DataMinerTesterServiceAsync service;

	@Inject
	OperatorsDiffPresenter(EventBus eventBus, PresenterView view, PresenterProxy proxy,
			DataMinerTesterServiceAsync service) {
		super(eventBus, view, proxy, ApplicationPresenter.SLOT_MAIN);
		this.service = service;
		getView().setUiHandlers(this);

	}

	@Override
	public void executeDiff(DMDiffConfig operatorsDiffConfig) {
		callOperatorsDiff(operatorsDiffConfig);
	}

	private void callOperatorsDiff(DMDiffConfig operatorsDiffConfig) {
		MaterialLoader.loading(true);
		service.getOperatorsDiff(getToken(), operatorsDiffConfig, new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				GWT.log("Error in diff: ", caught);
				String result = SafeHtmlUtils.htmlEscape(caught.getLocalizedMessage());
				MaterialLoader.loading(false);
				getView().setResult(result, false);
			}

			@Override
			public void onSuccess(String result) {
				MaterialLoader.loading(false);
				getView().setResult(result, true);
			}

		});

	}

	private String getToken() {
		String token = Window.Location.getParameter(Constants.TOKEN);
		GWT.log("Token: " + token);
		return token;
	}

}