package org.gcube.portlets.user.dataminermanagertester.client.rpc;

import org.gcube.data.analysis.dataminermanagercl.shared.data.computations.ComputationId;
import org.gcube.data.analysis.dataminermanagercl.shared.process.ComputationStatus.Status;
import org.gcube.portlets.user.dataminermanagertester.shared.config.DMBatchConfig;
import org.gcube.portlets.user.dataminermanagertester.shared.config.DMConfig;
import org.gcube.portlets.user.dataminermanagertester.shared.config.DMDiffConfig;
import org.gcube.portlets.user.dataminermanagertester.shared.exception.ServiceException;
import org.gcube.portlets.user.dataminermanagertester.shared.result.BatchTestResult;
import org.gcube.portlets.user.dataminermanagertester.shared.session.UserInfo;
import org.gcube.portlets.user.dataminermanagertester.shared.task.TaskStatus;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("dmtester")
public interface DataMinerTesterService extends RemoteService {

	public UserInfo hello(String token) throws ServiceException;

	public String getCapabilities(String token,DMConfig dmConfig) throws ServiceException;

	public String getOperatorsDiff(String token, DMDiffConfig operatorsDiffConfig) throws ServiceException;

	public ComputationId startSimpleTest(String token, DMConfig dmConfig) throws ServiceException;

	public String retrieveOutputForSimpleTest(String token, DMConfig dmConfig, ComputationId computationId) throws ServiceException;

	public Status monitorSimpleTest(String token, DMConfig dmConfig, ComputationId computationId) throws ServiceException;

	public String startBatchTest(String token, DMBatchConfig dmBatchConfig) throws ServiceException;

	public TaskStatus monitorBatchTest(String token, String operationId) throws ServiceException;

	public BatchTestResult retrieveOutputForBatchTest(String token, String operationId) throws ServiceException;
	
}
