package org.gcube.portlets.user.dataminermanagertester.client.application.testconfig;

import org.gcube.data.analysis.dataminermanagercl.shared.data.computations.ComputationId;
import org.gcube.data.analysis.dataminermanagercl.shared.process.ComputationStatus.Status;
import org.gcube.portlets.user.dataminermanagertester.client.application.ApplicationPresenter;
import org.gcube.portlets.user.dataminermanagertester.client.application.monitor.MonitorRequest;
import org.gcube.portlets.user.dataminermanagertester.client.application.monitor.MonitorRequestEvent;
import org.gcube.portlets.user.dataminermanagertester.client.application.monitor.MonitorRequestEvent.MonitorRequestEventHandler;
import org.gcube.portlets.user.dataminermanagertester.client.place.NameTokens;
import org.gcube.portlets.user.dataminermanagertester.client.rpc.DataMinerTesterServiceAsync;
import org.gcube.portlets.user.dataminermanagertester.shared.Constants;
import org.gcube.portlets.user.dataminermanagertester.shared.config.DMConfig;
import org.gcube.portlets.user.dataminermanagertester.shared.config.TestType;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.mvp.client.HasUiHandlers;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.NoGatekeeper;
import com.gwtplatform.mvp.client.annotations.ProxyStandard;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;

import gwt.material.design.client.ui.MaterialLoader;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
public class TestConfPresenter extends Presenter<TestConfPresenter.PresenterView, TestConfPresenter.PresenterProxy>
		implements TestConfUiHandlers {
	interface PresenterView extends View, HasUiHandlers<TestConfUiHandlers> {
		void setResult(String result, boolean success);

	}

	@ProxyStandard
	@NameToken(NameTokens.TEST)
	@NoGatekeeper
	interface PresenterProxy extends ProxyPlace<TestConfPresenter> {
	}

	private DataMinerTesterServiceAsync service;

	@Inject
	TestConfPresenter(EventBus eventBus, PresenterView view, PresenterProxy proxy,
			DataMinerTesterServiceAsync service) {
		super(eventBus, view, proxy, ApplicationPresenter.SLOT_MAIN);
		this.service = service;
		getView().setUiHandlers(this);

	}

	@Override
	public void executeTest(DMConfig dmConfig) {
		if (dmConfig.getTestType() != null && !dmConfig.getTestType().isEmpty()) {
			TestType testType = TestType.getTypeFromId(dmConfig.getTestType());
			switch (testType) {
			case Capabilities:
				callGetCapabilities(dmConfig);
				break;
			case FeedForwardAnn:
			case BionymLocal:
			case CMSY2:
			case CSquareColumnCreator:
			case DBScan:
			case ListDBName:
			case RasterDataPublisher:
			case WebAppPublisher:
			case XYExtractor:	
			case OpenMeshRecostructorGPU:
			case GenericCharts:
			case PolygonsToMap:	
				testSimple(dmConfig);
				break;
			default:
				break;

			}
		}

	}

	private void callGetCapabilities(DMConfig dmConfig) {
		MaterialLoader.loading(true);
		service.getCapabilities(getToken(), dmConfig, new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				GWT.log("Error get Capabilities: ", caught);
				String result = SafeHtmlUtils.htmlEscape(caught.getLocalizedMessage());
				MaterialLoader.loading(false);
				getView().setResult(result, false);
			}

			@Override
			public void onSuccess(String result) {
				MaterialLoader.loading(false);
				getView().setResult(result, true);
			}

		});

	}

	private void testSimple(DMConfig dmConfig) {
		MaterialLoader.loading(true);
		service.startSimpleTest(getToken(), dmConfig, new AsyncCallback<ComputationId>() {

			@Override
			public void onFailure(Throwable caught) {
				GWT.log("Error starting test: " + caught.getLocalizedMessage(), caught);
				String result = SafeHtmlUtils.htmlEscape(caught.getLocalizedMessage());
				StackTraceElement[] trace = caught.getStackTrace();
				for (int i = 0; i < trace.length; i++) {
					result += SafeHtmlUtils.htmlEscape(trace[i].toString()) + "\n";
				}
				MaterialLoader.loading(false);
				getView().setResult(result, false);
			}

			@Override
			public void onSuccess(ComputationId computationId) {
				monitorSimpleTest(dmConfig, computationId);

			}

		});

	}

	private void monitorSimpleTest(DMConfig dmConfig, ComputationId computationId) {
		final MonitorRequest monitorRequest = new MonitorRequest();
		MonitorRequestEventHandler handler = new MonitorRequestEventHandler() {

			@Override
			public void onMonitor(MonitorRequestEvent event) {
				service.monitorSimpleTest(getToken(), dmConfig, computationId, new AsyncCallback<Status>() {

					@Override
					public void onFailure(Throwable caught) {
						monitorRequest.stop();
						GWT.log("Error in test: " + caught.getLocalizedMessage(), caught);
						String result = SafeHtmlUtils.htmlEscape(caught.getLocalizedMessage());
						StackTraceElement[] trace = caught.getStackTrace();
						for (int i = 0; i < trace.length; i++) {
							result += SafeHtmlUtils.htmlEscape(trace[i].toString()) + "\n";
						}
						MaterialLoader.loading(false);
						getView().setResult(result, false);
					}

					@Override
					public void onSuccess(Status status) {
						if (status != null) {
							switch (status) {
							case COMPLETE:
								retrieveSimpleTestOutput(dmConfig, computationId);
								break;
							case CANCELLED:
							case FAILED:
								monitorRequest.stop();
								GWT.log("Error test failed");
								MaterialLoader.loading(false);
								getView().setResult("Error test failed", false);
								break;
							case RUNNING:
							case ACCEPTED:
							default:
								monitorRequest.repeat();
								break;

							}
						} else {
							monitorRequest.repeat();

						}

					}

				});
			}

		};

		monitorRequest.addHandler(handler);
		monitorRequest.start();

	}

	private void retrieveSimpleTestOutput(DMConfig dmConfig, ComputationId computationId) {
		service.retrieveOutputForSimpleTest(getToken(), dmConfig, computationId, new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {

				GWT.log("Error retrieving test output: " + caught.getLocalizedMessage(), caught);
				String result = SafeHtmlUtils.htmlEscape(caught.getLocalizedMessage());
				StackTraceElement[] trace = caught.getStackTrace();
				for (int i = 0; i < trace.length; i++) {
					result += SafeHtmlUtils.htmlEscape(trace[i].toString()) + "\n";
				}
				MaterialLoader.loading(false);
				getView().setResult(result, false);
			}

			@Override
			public void onSuccess(String result) {
				MaterialLoader.loading(false);
				getView().setResult(result, true);

			}

		});

	}

	private String getToken() {
		String token = Window.Location.getParameter(Constants.TOKEN);
		GWT.log("Token: " + token);
		return token;
	}

}