package org.gcube.portlets.user.dataminermanagertester.server;


import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
  * @author Giancarlo Panichi
 *
 *
 */
@WebListener
public class DataMinerTesterListener implements ServletContextListener {

	private static Logger logger = LoggerFactory
			.getLogger(DataMinerTesterListener.class);

	private DataMinerTesterBatchDaemon dataMinerTesterBatchDaemon = null;
	private Thread thread = null;

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		
		dataMinerTesterBatchDaemon = new DataMinerTesterBatchDaemon(sce);
		thread = new Thread(dataMinerTesterBatchDaemon);
		logger.debug("Starting DataMinerTesterBatchDaemon: " + thread);
		thread.start();
		logger.info("DataMinerTesterBatchDaemon process successfully started.");
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		logger.debug("Stopping DataMinerTesterBatchDaemon: " + thread);
		if (thread != null) {
			dataMinerTesterBatchDaemon.terminate();
			try {
				thread.join();
			} catch (InterruptedException e) {
			}
			logger.debug("DataMinerTesterBatchDaemon successfully stopped.");
		}
		
	}

}