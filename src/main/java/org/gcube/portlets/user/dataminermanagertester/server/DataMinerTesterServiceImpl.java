package org.gcube.portlets.user.dataminermanagertester.server;

import java.util.HashMap;
import java.util.List;
import java.util.Queue;
import java.util.UUID;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.gcube.data.analysis.dataminermanagercl.server.DataMinerService;
import org.gcube.data.analysis.dataminermanagercl.server.dmservice.SClient;
import org.gcube.data.analysis.dataminermanagercl.server.util.ServiceCredentials;
import org.gcube.data.analysis.dataminermanagercl.shared.data.OutputData;
import org.gcube.data.analysis.dataminermanagercl.shared.data.computations.ComputationId;
import org.gcube.data.analysis.dataminermanagercl.shared.parameters.Parameter;
import org.gcube.data.analysis.dataminermanagercl.shared.process.ComputationStatus;
import org.gcube.data.analysis.dataminermanagercl.shared.process.ComputationStatus.Status;
import org.gcube.data.analysis.dataminermanagercl.shared.process.Operator;
import org.gcube.data.analysis.dataminermanagercl.shared.process.OperatorsClassification;
import org.gcube.portlets.user.dataminermanagertester.client.rpc.DataMinerTesterService;
import org.gcube.portlets.user.dataminermanagertester.server.task.TaskRequest;
import org.gcube.portlets.user.dataminermanagertester.server.task.TaskWrapper;
import org.gcube.portlets.user.dataminermanagertester.server.testbuild.TestBuilder;
import org.gcube.portlets.user.dataminermanagertester.server.testconfig.DMTest;
import org.gcube.portlets.user.dataminermanagertester.shared.config.DMBatchConfig;
import org.gcube.portlets.user.dataminermanagertester.shared.config.DMConfig;
import org.gcube.portlets.user.dataminermanagertester.shared.config.DMDiffConfig;
import org.gcube.portlets.user.dataminermanagertester.shared.config.TestType;
import org.gcube.portlets.user.dataminermanagertester.shared.exception.ServiceException;
import org.gcube.portlets.user.dataminermanagertester.shared.result.BatchTestResult;
import org.gcube.portlets.user.dataminermanagertester.shared.session.UserInfo;
import org.gcube.portlets.user.dataminermanagertester.shared.task.TaskStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
@SuppressWarnings("serial")
public class DataMinerTesterServiceImpl extends RemoteServiceServlet implements DataMinerTesterService {

	private static Logger logger = LoggerFactory.getLogger(DataMinerTesterServiceImpl.class);

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void init() throws ServletException {
		super.init();
		logger.info("DataMiner Tester Service started!");

	}

	/**
	 * 
	 * {@inheritDoc}
	 * 
	 */
	@Override
	public UserInfo hello(String token) throws ServiceException {
		try {
			ServiceCredentials serviceCredentials = SessionUtil.getServiceCredentials(this.getThreadLocalRequest(),
					token);
			logger.debug("hello()");
			UserInfo userInfo = new UserInfo(serviceCredentials.getUserName(), serviceCredentials.getGroupId(),
					serviceCredentials.getGroupName(), serviceCredentials.getScope(), serviceCredentials.getEmail(),
					serviceCredentials.getFullName());
			logger.debug("UserInfo: " + userInfo);
			return userInfo;
		} catch (ServiceException e) {
			e.printStackTrace();
			throw e;
		} catch (Throwable e) {
			e.printStackTrace();
			logger.error("Hello(): " + e.getLocalizedMessage(), e);
			throw new ServiceException(e.getLocalizedMessage(), e);
		}

	}

	@Override
	public String getCapabilities(String token, DMConfig dmConfig) throws ServiceException {

		try {
			HttpServletRequest httpRequest = this.getThreadLocalRequest();
			@SuppressWarnings("unused")
			ServiceCredentials serviceCredentials = SessionUtil.getServiceCredentials(httpRequest, token);	
			logger.debug("getCapabilities()");
			
			SClient sClient = new DataMinerService().getClient(dmConfig.getToken(), dmConfig.getDmUrl());
			
			List<OperatorsClassification> operatorsClassifications = sClient.getOperatorsClassifications();
			logger.debug("OperatorsClassifications: " + operatorsClassifications);

			StringBuilder result = new StringBuilder();

			result.append("Success\n");

			for (OperatorsClassification opClass : operatorsClassifications) {
				for (Operator op : opClass.getOperators()) {
					result.append("Operator[Name=" + op.getName() + "]");
				}
			}

			return result.toString();
		} catch (ServiceException e) {
			logger.error(e.getLocalizedMessage());
			throw e;
		} catch (Throwable e) {
			logger.error("An error occurred getting the Capabilities: " + e.getLocalizedMessage(), e);
			throw new ServiceException(e.getLocalizedMessage(), e);
		}
	}

	@Override
	public String getOperatorsDiff(String token, DMDiffConfig operatorsDiffConfig) throws ServiceException {
		try {
			logger.debug("getOperatorsDiff: dm1=" + operatorsDiffConfig.getDm1() + ", dm2="
					+ operatorsDiffConfig.getDm2());
			HttpServletRequest httpRequest = this.getThreadLocalRequest();
			@SuppressWarnings("unused")
			ServiceCredentials serviceCredentials = SessionUtil.getServiceCredentials(httpRequest, token);

			SClient sClient1 = new DataMinerService().getClient(operatorsDiffConfig.getToken1(),
					operatorsDiffConfig.getDmUrl1());
			
			List<OperatorsClassification> operatorsClassifications1 = sClient1.getOperatorsClassifications();
			logger.debug("OperatorsClassifications1: " + operatorsClassifications1);

			SClient sClient2 = new DataMinerService().getClient(operatorsDiffConfig.getToken2(),
					operatorsDiffConfig.getDmUrl2());
			List<OperatorsClassification> operatorsClassifications2 = sClient2.getOperatorsClassifications();
			logger.debug("OperatorsClassifications2: " + operatorsClassifications2);

			StringBuilder result = new StringBuilder();

			result.append("Success\n");

			result.append("----------------------------------\n");
			result.append("Operators in: " + operatorsDiffConfig.getDm1() + " \n");
			result.append("and not in: " + operatorsDiffConfig.getDm2() + " \n");
			for (OperatorsClassification opClass1 : operatorsClassifications1) {
				for (Operator op1 : opClass1.getOperators()) {
					boolean op1Found = false;
					for (OperatorsClassification opClass2 : operatorsClassifications2) {
						for (Operator op2 : opClass2.getOperators()) {
							if (op1.getId().compareTo(op2.getId()) == 0) {
								op1Found = true;
								break;
							}
						}
						if (op1Found) {
							break;
						}

					}
					if (!op1Found) {
						result.append("Op: " + op1.getId() + " \n");
					}
				}

			}

			result.append("----------------------------------\n");
			result.append("Operators in: " + operatorsDiffConfig.getDm2() + " \n");
			result.append("and not in: " + operatorsDiffConfig.getDm1() + " \n");
			for (OperatorsClassification opClass2 : operatorsClassifications2) {
				for (Operator op2 : opClass2.getOperators()) {
					boolean op2Found = false;
					for (OperatorsClassification opClass1 : operatorsClassifications1) {
						for (Operator op1 : opClass1.getOperators()) {
							if (op2.getId().compareTo(op1.getId()) == 0) {
								op2Found = true;
								break;
							}
						}
						if (op2Found) {
							break;
						}

					}
					if (!op2Found) {
						result.append("Op: " + op2.getId() + " \n");
					}
				}

			}

			return result.toString();

		} catch (ServiceException e) {
			logger.error(e.getLocalizedMessage());
			throw e;
		} catch (Throwable e) {
			logger.error("An error occurred getting operators diff: " + e.getLocalizedMessage(), e);
			throw new ServiceException(e.getLocalizedMessage(), e);
		}

	}

	@Override
	public ComputationId startSimpleTest(String token, DMConfig dmConfig) throws ServiceException {

		try {
			HttpServletRequest httpRequest = this.getThreadLocalRequest();
			@SuppressWarnings("unused")
			ServiceCredentials serviceCredentials = SessionUtil.getServiceCredentials(httpRequest, token);
			logger.debug("startSimpleTest()");
			TestType t = null;
			if (dmConfig == null || dmConfig.getTestType() == null || dmConfig.getTestType().isEmpty()) {
				throw new ServiceException("Invalid test type: " + dmConfig.getTestType());
			} else {
				t = TestType.getTypeFromId(dmConfig.getTestType());
			}

			TestBuilder testBuilder = new TestBuilder();
			DMTest dmTest = testBuilder.build(t);

			SClient sClient = new DataMinerService().getClient(dmConfig.getToken(), dmConfig.getDmUrl());
			
			List<OperatorsClassification> operatorsClassifications = sClient.getOperatorsClassifications();
			logger.debug("OperatorsClassifications: " + operatorsClassifications);

			Operator operator = sClient.getOperatorById(dmTest.getId());

			if (operator == null) {
				logger.error("Operator not found");
				throw new ServiceException("Operator: " + dmTest.getId() + " not found");
			} else {
				logger.debug("Operator Name: " + operator.getName() + " (" + operator.getId() + ")");
				logger.debug("Operator: " + operator);
				List<Parameter> parameters = sClient.getInputParameters(operator);
				logger.debug("Parameters: " + parameters);
				for (Parameter parameter : parameters) {
					logger.debug(
							"Parameter:[Name=" + parameter.getName() + ", Typology=" + parameter.getTypology() + "]");
				}

				dmTest.createRequest(operator);
				logger.debug("Start Computation");
				ComputationId computationId = sClient.startComputation(operator);
				logger.debug("Started ComputationId: " + computationId);
				return computationId;
			}

		} catch (ServiceException e) {
			logger.error(e.getLocalizedMessage());
			throw e;
		} catch (Throwable e) {
			logger.error("An error occurred starting simple test: " + e.getLocalizedMessage(), e);
			e.printStackTrace();
			throw new ServiceException(e.getLocalizedMessage(), e);
		}
	}

	@Override
	public Status monitorSimpleTest(String token, DMConfig dmConfig, ComputationId computationId)
			throws ServiceException {

		try {
			HttpServletRequest httpRequest = this.getThreadLocalRequest();
			@SuppressWarnings("unused")
			ServiceCredentials serviceCredentials = SessionUtil.getServiceCredentials(httpRequest, token);
			
			SClient sClient = new DataMinerService().getClient(dmConfig.getToken(), dmConfig.getDmUrl());

			logger.debug("Requesting operation progress");
			ComputationStatus computationStatus = null;
			try {
				computationStatus = sClient.getComputationStatus(computationId);
			} catch (Exception e) {
				logger.error("Error retrieving computation Status:" + e.getLocalizedMessage(), e);
				throw new ServiceException("Error retrieving computation Status:" + e.getLocalizedMessage(), e);

			}
			logger.debug("ComputationStatus: " + computationStatus);
			if (computationStatus == null) {
				logger.error("ComputationStatus is null");
				throw new ServiceException("Error retrieving computation Status: ComputationStatus is null");
			}

			Status status = computationStatus.getStatus();
			if (status == null) {
				logger.error("Status is null");
				throw new ServiceException("Error retrieving computation Status: Status is null");
			}

			return status;

			// monitoringComputation(computationId, sClient);
		} catch (ServiceException e) {
			logger.error(e.getLocalizedMessage());
			throw e;
		} catch (Throwable e) {
			logger.error("An error occurred monitoring simple test: " + e.getLocalizedMessage(), e);
			e.printStackTrace();
			throw new ServiceException(e.getLocalizedMessage(), e);
		}
	}

	@Override
	public String retrieveOutputForSimpleTest(String token, DMConfig dmConfig, ComputationId computationId)
			throws ServiceException {

		try {
			HttpServletRequest httpRequest = this.getThreadLocalRequest();
			@SuppressWarnings("unused")
			ServiceCredentials serviceCredentials = SessionUtil.getServiceCredentials(httpRequest, token);
			logger.debug("retrieveOutputForSimpleTest()");
			
			TestType t = null;
			if (dmConfig == null || dmConfig.getTestType() == null || dmConfig.getTestType().isEmpty()) {
				throw new ServiceException("Invalid test type: " + dmConfig.getTestType());
			} else {
				t = TestType.getTypeFromId(dmConfig.getTestType());
			}

			TestBuilder testBuilder = new TestBuilder();
			DMTest dmTest = testBuilder.build(t);
			
			SClient sClient = new DataMinerService().getClient(dmConfig.getToken(), dmConfig.getDmUrl());

			OutputData outputData = sClient.getOutputDataByComputationId(computationId);

			if (dmTest.isValidResult(outputData)) {
				return dmTest.getResult(outputData);
			} else {
				throw new ServiceException("Invalid output data for test: " + dmTest.getId());
			}

		} catch (ServiceException e) {
			logger.error(e.getLocalizedMessage());
			throw e;
		} catch (Throwable e) {
			logger.error("An error occurred monitoring simple test: " + e.getLocalizedMessage(), e);
			e.printStackTrace();
			throw new ServiceException(e.getLocalizedMessage(), e);
		}
	}

	@Override
	public String startBatchTest(String token, DMBatchConfig dmBatchConfig) throws ServiceException {

		try {
			HttpServletRequest httpRequest = this.getThreadLocalRequest();
			ServiceCredentials serviceCredentials = SessionUtil.getServiceCredentials(httpRequest, token);
			logger.debug("startBatchTest()");
			
			String operationId = UUID.randomUUID().toString();
			logger.info("DataMinerBatchTest Task Operation Id: " + operationId);
			logger.info("Session Id: " + httpRequest.getSession());

			TaskRequest taskRequest = new TaskRequest(operationId, httpRequest.getSession(), serviceCredentials,
					dmBatchConfig);

			ServletContext appScope = httpRequest.getServletContext();

			@SuppressWarnings("unchecked")
			Queue<TaskRequest> queue = ((Queue<TaskRequest>) appScope
					.getAttribute(SessionConstants.TASK_REQUEST_QUEUE));

			queue.add(taskRequest);
			logger.debug("operationId: " + operationId);
			return operationId;

		} catch (ServiceException e) {
			logger.error(e.getLocalizedMessage());
			throw e;
		} catch (Throwable e) {
			logger.error("An error occurred starting batch test: " + e.getLocalizedMessage(), e);
			e.printStackTrace();
			throw new ServiceException(e.getLocalizedMessage(), e);
		}
	}

	@Override
	public TaskStatus monitorBatchTest(String token, String operationId) throws ServiceException {
		try {
			HttpServletRequest httpRequest = this.getThreadLocalRequest();
			ServiceCredentials serviceCredentials = SessionUtil.getServiceCredentials(httpRequest, token);
			
			HashMap<String, TaskWrapper> taskWrapperMap = SessionUtil.getTaskWrapperMap(httpRequest.getSession(),
					serviceCredentials);
			if (taskWrapperMap == null) {
				return null;
			} else {
				if (taskWrapperMap.containsKey(operationId)) {
					TaskWrapper taskWrapper = taskWrapperMap.get(operationId);
					return taskWrapper.getTaskStatus();
				} else {
					return null;
				}
			}

		} catch (ServiceException e) {
			e.printStackTrace();
			throw e;
		} catch (Throwable e) {
			logger.error("MonitorBatchTest(): " + e.getLocalizedMessage(), e);
			throw new ServiceException(e.getLocalizedMessage());
		}

	}

	@Override
	public BatchTestResult retrieveOutputForBatchTest(String token, String operationId) throws ServiceException {
		try {
			HttpServletRequest httpRequest = this.getThreadLocalRequest();
			ServiceCredentials serviceCredentials = SessionUtil.getServiceCredentials(httpRequest, token);
			logger.debug("retrieveOutputForBatchTest()");
			
			HashMap<String, TaskWrapper> taskWrapperMap = SessionUtil.getTaskWrapperMap(httpRequest.getSession(),
					serviceCredentials);
			if (taskWrapperMap == null) {
				return null;
			} else {
				if (taskWrapperMap.containsKey(operationId)) {
					TaskWrapper taskWrapper = taskWrapperMap.get(operationId);
					return taskWrapper.getResult();
				} else {
					return null;
				}
			}

		} catch (ServiceException e) {
			e.printStackTrace();
			throw e;
		} catch (Throwable e) {
			logger.error("RetrieveOutputForBatchTest(): " + e.getLocalizedMessage(), e);
			throw new ServiceException(e.getLocalizedMessage());
		}

	}

}
