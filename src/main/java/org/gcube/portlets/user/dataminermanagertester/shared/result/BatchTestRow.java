package org.gcube.portlets.user.dataminermanagertester.shared.result;

import java.io.Serializable;

import org.gcube.data.analysis.dataminermanagercl.shared.process.ComputationStatus.Status;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class BatchTestRow implements Serializable {

	private static final long serialVersionUID = -8991094616615074608L;
	private String url;
	private Status status;
	private String response;

	public BatchTestRow() {
		super();
	}

	public BatchTestRow(String url, Status status, String response) {
		super();
		this.url = url;
		this.status = status;
		this.response = response;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	@Override
	public String toString() {
		return "BatchTestRow [url=" + url + ", status=" + status + ", response=" + response + "]";
	}

}
