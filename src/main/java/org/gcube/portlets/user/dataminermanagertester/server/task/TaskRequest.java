package org.gcube.portlets.user.dataminermanagertester.server.task;

import java.io.Serializable;

import javax.servlet.http.HttpSession;

import org.gcube.data.analysis.dataminermanagercl.server.util.ServiceCredentials;
import org.gcube.portlets.user.dataminermanagertester.shared.config.DMBatchConfig;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class TaskRequest implements Serializable {

	private static final long serialVersionUID = -4010108343968344171L;
	private String operationId;
	private HttpSession httpSession;
	private ServiceCredentials serviceCredentials;
	private DMBatchConfig dmBatchConfig;

	public TaskRequest(String operationId, HttpSession httpSession, ServiceCredentials serviceCredentials,
			DMBatchConfig dmBatchConfig) {
		super();
		this.operationId = operationId;
		this.httpSession = httpSession;
		this.serviceCredentials = serviceCredentials;
		this.dmBatchConfig = dmBatchConfig;

	}

	public String getOperationId() {
		return operationId;
	}

	public void setOperationId(String operationId) {
		this.operationId = operationId;
	}

	public HttpSession getHttpSession() {
		return httpSession;
	}

	public void setHttpSession(HttpSession httpSession) {
		this.httpSession = httpSession;
	}

	public ServiceCredentials getServiceCredentials() {
		return serviceCredentials;
	}

	public void setServiceCredentials(ServiceCredentials serviceCredentials) {
		this.serviceCredentials = serviceCredentials;
	}

	public DMBatchConfig getDmBatchConfig() {
		return dmBatchConfig;
	}

	public void setDmBatchConfig(DMBatchConfig dmBatchConfig) {
		this.dmBatchConfig = dmBatchConfig;
	}

	@Override
	public String toString() {
		return "TaskRequest [operationId=" + operationId + ", httpSession=" + httpSession + ", serviceCredentials="
				+ serviceCredentials + ", dmBatchConfig=" + dmBatchConfig + "]";
	}

}
