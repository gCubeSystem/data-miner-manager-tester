package org.gcube.portlets.user.dataminermanagertester.client.application.testbatchconfig;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.gcube.data.analysis.dataminermanagercl.shared.process.ComputationStatus.Status;
import org.gcube.portlets.user.dataminermanagertester.client.application.ApplicationPresenter;
import org.gcube.portlets.user.dataminermanagertester.client.application.monitor.MonitorRequest;
import org.gcube.portlets.user.dataminermanagertester.client.application.monitor.MonitorRequestEvent;
import org.gcube.portlets.user.dataminermanagertester.client.application.monitor.MonitorRequestEvent.MonitorRequestEventHandler;
import org.gcube.portlets.user.dataminermanagertester.client.place.NameTokens;
import org.gcube.portlets.user.dataminermanagertester.client.rpc.DataMinerTesterServiceAsync;
import org.gcube.portlets.user.dataminermanagertester.shared.Constants;
import org.gcube.portlets.user.dataminermanagertester.shared.config.DMBatchConfig;
import org.gcube.portlets.user.dataminermanagertester.shared.result.BatchTestResult;
import org.gcube.portlets.user.dataminermanagertester.shared.result.BatchTestRow;
import org.gcube.portlets.user.dataminermanagertester.shared.result.SingleTestResult;
import org.gcube.portlets.user.dataminermanagertester.shared.task.TaskStatus;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.mvp.client.HasUiHandlers;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.NoGatekeeper;
import com.gwtplatform.mvp.client.annotations.ProxyStandard;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;

import gwt.material.design.client.ui.MaterialLoader;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
public class TestBatchConfPresenter
		extends Presenter<TestBatchConfPresenter.PresenterView, TestBatchConfPresenter.PresenterProxy>
		implements TestBatchConfUiHandlers {
	interface PresenterView extends View, HasUiHandlers<TestBatchConfUiHandlers> {
		void setResult(String result, boolean success);

		void setupTable();

		void setResultTable(ArrayList<BatchTestRow> batchTestRows);

		void setupOptions();

	}

	@ProxyStandard
	@NameToken(NameTokens.BATCH)
	@NoGatekeeper
	interface PresenterProxy extends ProxyPlace<TestBatchConfPresenter> {
	}

	private DataMinerTesterServiceAsync service;

	@Inject
	TestBatchConfPresenter(EventBus eventBus, PresenterView view, PresenterProxy proxy,
			DataMinerTesterServiceAsync service) {
		super(eventBus, view, proxy, ApplicationPresenter.SLOT_MAIN);
		this.service = service;
		getView().setUiHandlers(this);

	}

	@Override
	protected void onBind() {
		super.onBind();
		getView().setupOptions();
		getView().setupTable();
	}

	@Override
	public void executeBatchTest(DMBatchConfig dmBatchConfig) {
		if (dmBatchConfig.getTestType() != null && !dmBatchConfig.getTestType().isEmpty()) {
			testBatch(dmBatchConfig);
		}

	}

	private void testBatch(DMBatchConfig dmBatchConfig) {
		MaterialLoader.loading(true);
		service.startBatchTest(getToken(), dmBatchConfig, new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				GWT.log("Error starting test: " + caught.getLocalizedMessage(), caught);
				String result = SafeHtmlUtils.htmlEscape(caught.getLocalizedMessage());
				StackTraceElement[] trace = caught.getStackTrace();
				for (int i = 0; i < trace.length; i++) {
					result += SafeHtmlUtils.htmlEscape(trace[i].toString()) + "\n";
				}
				MaterialLoader.loading(false);
				getView().setResult(result, false);
			}

			@Override
			public void onSuccess(String computationId) {
				monitorBatchTest(dmBatchConfig, computationId);

			}

		});

	}

	private void monitorBatchTest(DMBatchConfig dmBatchConfig, String computationId) {
		final MonitorRequest monitorRequest = new MonitorRequest();
		MonitorRequestEventHandler handler = new MonitorRequestEventHandler() {

			@Override
			public void onMonitor(MonitorRequestEvent event) {
				service.monitorBatchTest(getToken(), computationId, new AsyncCallback<TaskStatus>() {

					@Override
					public void onFailure(Throwable caught) {
						monitorRequest.stop();
						GWT.log("Error in test: " + caught.getLocalizedMessage(), caught);
						String result = SafeHtmlUtils.htmlEscape(caught.getLocalizedMessage());
						StackTraceElement[] trace = caught.getStackTrace();
						for (int i = 0; i < trace.length; i++) {
							result += SafeHtmlUtils.htmlEscape(trace[i].toString()) + "\n";
						}
						MaterialLoader.loading(false);
						getView().setResult(result, false);
					}

					@Override
					public void onSuccess(TaskStatus taskStatus) {
						if (taskStatus != null) {
							switch (taskStatus) {
							case COMPLETED:
								retrieveBatchTestOutput(dmBatchConfig, computationId);
								break;
							case ERROR:
								monitorRequest.stop();
								GWT.log("Error test failed");
								MaterialLoader.loading(false);
								getView().setResult("Error test failed", false);
								break;
							case RUNNING:
							case STARTED:
							default:
								monitorRequest.repeat();
								break;
							}
						} else {
							monitorRequest.repeat();
						}
					}

				});
			}

		};

		monitorRequest.addHandler(handler);
		monitorRequest.start();

	}

	private void retrieveBatchTestOutput(DMBatchConfig dmBatchConfig, String computationId) {
		service.retrieveOutputForBatchTest(getToken(), computationId, new AsyncCallback<BatchTestResult>() {

			@Override
			public void onFailure(Throwable caught) {

				GWT.log("Error retrieving test output: " + caught.getLocalizedMessage(), caught);
				String result = SafeHtmlUtils.htmlEscape(caught.getLocalizedMessage());
				StackTraceElement[] trace = caught.getStackTrace();
				for (int i = 0; i < trace.length; i++) {
					result += SafeHtmlUtils.htmlEscape(trace[i].toString()) + "\n";
				}
				MaterialLoader.loading(false);
				getView().setResult(result, false);
			}

			@Override
			public void onSuccess(BatchTestResult batchTestResult) {
				MaterialLoader.loading(false);
				ArrayList<BatchTestRow> batchTestRows = new ArrayList<>();
				LinkedHashMap<String, SingleTestResult> testResultMap = batchTestResult.getTestResultMap();
				for (String url : testResultMap.keySet()) {
					SingleTestResult singleTestResult = testResultMap.get(url);
					Status status = singleTestResult.getComputationStatus().getStatus();
					BatchTestRow batchTestRow;
					if (status.compareTo(Status.COMPLETE) == 0) {
						batchTestRow = new BatchTestRow(url, status, singleTestResult.getResponse());
					} else {
						Exception e = singleTestResult.getComputationStatus().getError();
						batchTestRow = new BatchTestRow(url, status, e.getLocalizedMessage());
					}
					batchTestRows.add(batchTestRow);

				}
				getView().setResultTable(batchTestRows);

			}

		});

	}

	private String getToken() {
		String token = Window.Location.getParameter(Constants.TOKEN);
		GWT.log("Token: " + token);
		return token;
	}

}