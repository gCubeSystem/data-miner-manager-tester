package org.gcube.portlets.user.dataminermanagertester.server.task;

import java.io.Serializable;

import org.gcube.portlets.user.dataminermanagertester.shared.result.BatchTestResult;
import org.gcube.portlets.user.dataminermanagertester.shared.task.TaskStatus;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class TaskWrapper implements Serializable {

	private static final long serialVersionUID = -4010108343968344171L;
	private String operationId;
	private TaskStatus taskStatus;
	private String errorMessage;
	private BatchTestResult result;

	public TaskWrapper(String operationId, TaskStatus taskStatus, BatchTestResult result) {
		super();
		this.operationId = operationId;
		this.taskStatus = taskStatus;
		this.errorMessage = null;
		this.result = result;
	}

	public TaskWrapper(String operationId, TaskStatus taskStatus, String errorMessage) {
		super();
		this.operationId = operationId;
		this.taskStatus = taskStatus;
		this.errorMessage = errorMessage;
		this.result = null;
	}

	public String getOperationId() {
		return operationId;
	}

	public void setOperationId(String operationId) {
		this.operationId = operationId;
	}

	public TaskStatus getTaskStatus() {
		return taskStatus;
	}

	public void setTaskStatus(TaskStatus taskStatus) {
		this.taskStatus = taskStatus;
	}

	public BatchTestResult getResult() {
		return result;
	}

	public void setResult(BatchTestResult result) {
		this.result = result;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	@Override
	public String toString() {
		return "TaskWrapper [operationId=" + operationId + ", taskStatus=" + taskStatus + ", errorMessage="
				+ errorMessage + ", result=" + result + "]";
	}

}
