package org.gcube.portlets.user.dataminermanagertester.shared.config;

import java.io.Serializable;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class DMDiffConfig implements Serializable {

	private static final long serialVersionUID = -8892345776441085380L;
	private String dm1;
	private String token1;
	private String dm2;
	private String token2;
	private String protocol;

	public DMDiffConfig() {
		super();
	}

	public DMDiffConfig(String dm1, String token1, String dm2, String token2, String protocol) {
		super();
		this.dm1 = dm1;
		this.token1 = token1;
		this.dm2 = dm2;
		this.token2 = token2;
		this.protocol = protocol;
	}

	public String getDm1() {
		return dm1;
	}

	public void setDm1(String dm1) {
		this.dm1 = dm1;
	}

	public String getDmUrl1() {
		String url=null;
		if (protocol != null && dm1 != null) {
			url = new String(protocol + "://" + dm1 + "/wps/");
		}
		return url;
	}

	public String getToken1() {
		return token1;
	}

	public void setToken1(String token1) {
		this.token1 = token1;
	}

	public String getDm2() {
		return dm2;
	}

	public void setDm2(String dm2) {
		this.dm2 = dm2;
	}

	public String getDmUrl2() {
		String url=null;
		if (protocol != null && dm2 != null) {
			url = new String(protocol + "://" + dm2 + "/wps/");
		}
		return url;
	}

	public String getToken2() {
		return token2;
	}

	public void setToken2(String token2) {
		this.token2 = token2;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	@Override
	public String toString() {
		return "DMDiffConfig [dm1=" + dm1 + ", token1=" + token1 + ", dm2=" + dm2 + ", token2=" + token2 + ", protocol="
				+ protocol + "]";
	}

}
