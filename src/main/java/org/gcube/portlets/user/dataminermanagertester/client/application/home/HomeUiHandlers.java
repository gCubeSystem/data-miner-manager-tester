package org.gcube.portlets.user.dataminermanagertester.client.application.home;


import com.gwtplatform.mvp.client.UiHandlers;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
public interface HomeUiHandlers extends UiHandlers{

}
