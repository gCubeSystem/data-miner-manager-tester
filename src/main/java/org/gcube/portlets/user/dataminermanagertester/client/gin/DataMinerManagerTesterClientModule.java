package org.gcube.portlets.user.dataminermanagertester.client.gin;

import com.gwtplatform.mvp.client.gin.AbstractPresenterModule;
import com.gwtplatform.mvp.client.gin.DefaultModule;

import org.gcube.portlets.user.dataminermanagertester.client.application.ApplicationModule;
import org.gcube.portlets.user.dataminermanagertester.client.place.NameTokens;
import org.gcube.portlets.user.dataminermanagertester.client.resources.ResourceLoader;
import org.gcube.portlets.user.dataminermanagertester.client.rpc.DataMinerTesterServiceAsync;

import com.google.inject.Singleton;
import com.gwtplatform.mvp.client.RootPresenter;
import com.gwtplatform.mvp.client.annotations.DefaultPlace;
import com.gwtplatform.mvp.client.annotations.ErrorPlace;
import com.gwtplatform.mvp.client.annotations.UnauthorizedPlace;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class DataMinerManagerTesterClientModule extends AbstractPresenterModule {

	@Override
	protected void configure() {		
		bind(ResourceLoader.class).asEagerSingleton();
		bind(DataMinerTesterServiceAsync.class).in(Singleton.class);
		bind(RootPresenter.class).to(CustomRootPresenter.class).asEagerSingleton();
		
		install(new DefaultModule());
		install(new ApplicationModule());

		
		// DefaultPlaceManager Places
		bindConstant().annotatedWith(DefaultPlace.class).to(NameTokens.TEST);
		bindConstant().annotatedWith(ErrorPlace.class).to(NameTokens.TEST);
		bindConstant().annotatedWith(UnauthorizedPlace.class).to(NameTokens.TEST);
	}

}