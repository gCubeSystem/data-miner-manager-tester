package org.gcube.portlets.user.dataminermanagertester.server.testbatch;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.gcube.data.analysis.dataminermanagercl.server.DataMinerService;
import org.gcube.data.analysis.dataminermanagercl.server.dmservice.SClient;
import org.gcube.data.analysis.dataminermanagercl.shared.data.OutputData;
import org.gcube.data.analysis.dataminermanagercl.shared.data.computations.ComputationId;
import org.gcube.data.analysis.dataminermanagercl.shared.parameters.Parameter;
import org.gcube.data.analysis.dataminermanagercl.shared.process.ComputationStatus;
import org.gcube.data.analysis.dataminermanagercl.shared.process.ComputationStatus.Status;
import org.gcube.data.analysis.dataminermanagercl.shared.process.Operator;
import org.gcube.data.analysis.dataminermanagercl.shared.process.OperatorsClassification;
import org.gcube.portlets.user.dataminermanagertester.server.task.TaskRequest;
import org.gcube.portlets.user.dataminermanagertester.server.testbuild.TestBuilder;
import org.gcube.portlets.user.dataminermanagertester.server.testconfig.DMTest;
import org.gcube.portlets.user.dataminermanagertester.shared.Constants;
import org.gcube.portlets.user.dataminermanagertester.shared.config.DMBatchConfig;
import org.gcube.portlets.user.dataminermanagertester.shared.config.TestType;
import org.gcube.portlets.user.dataminermanagertester.shared.exception.ServiceException;
import org.gcube.portlets.user.dataminermanagertester.shared.result.BatchTestResult;
import org.gcube.portlets.user.dataminermanagertester.shared.result.SingleTestResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class TesterCaller {

	private static Logger logger = LoggerFactory.getLogger(TesterCaller.class);

	private DMTest dmTest;
	private DMBatchConfig dmBatchconfig;
	private ArrayList<String> dms;

	public BatchTestResult runTest(TaskRequest taskRequest) throws ServiceException {

		TestType t = null;
		if (taskRequest == null || taskRequest.getDmBatchConfig() == null
				|| taskRequest.getDmBatchConfig().getTestType() == null
				|| taskRequest.getDmBatchConfig().getTestType().isEmpty()) {
			throw new ServiceException("Invalid test type: " + taskRequest.getDmBatchConfig().getTestType());
		} else {
			t = TestType.getTypeFromId(taskRequest.getDmBatchConfig().getTestType());
		}

		dmBatchconfig = taskRequest.getDmBatchConfig();
		dms = taskRequest.getDmBatchConfig().getDms();

		if (dms == null || dms.isEmpty()) {
			logger.error("Invalid urls list");
			throw new ServiceException("Invalid urls list");
		}

		BatchTestResult result;

		if (t.compareTo(TestType.Capabilities) == 0) {
			result = runGetCapabilities();
		} else {
			result = runAlgorithms(t);
		}

		return result;
	}

	private BatchTestResult runGetCapabilities() {
		BatchTestResult result;
		LinkedHashMap<String, SingleTestResult> testResultMap = new LinkedHashMap<>();
		for (String dm : dms) {
			try {
				String url = null;
				if (dmBatchconfig.getProtocol() != null && dm != null) {
					dm = dm.trim();
					url = new String(dmBatchconfig.getProtocol() + "://" + dm + "/wps/");
				}

				SClient sClient = new DataMinerService().getClient(dmBatchconfig.getToken(), url);

				List<OperatorsClassification> operatorsClassifications = sClient.getOperatorsClassifications();
				logger.debug("OperatorsClassifications: " + operatorsClassifications);

				String response = new String("Success");

				ComputationStatus computationStatus = new ComputationStatus(Status.COMPLETE, 100f);
				SingleTestResult singleTestResult = new SingleTestResult(computationStatus, response);
				testResultMap.put(dm, singleTestResult);
			} catch (Exception e) {
				logger.error("Error testing: " + dm);
				ComputationStatus computationStatus = new ComputationStatus(e);
				SingleTestResult singleTestResult = new SingleTestResult(computationStatus, null);
				testResultMap.put(dm, singleTestResult);
			}
		}
		result = new BatchTestResult(testResultMap);
		return result;
	}

	private BatchTestResult runAlgorithms(TestType t) throws ServiceException {
		BatchTestResult result;
		TestBuilder testBuilder = new TestBuilder();
		dmTest = testBuilder.build(t);

		LinkedHashMap<String, SingleTestResult> testResultMap = new LinkedHashMap<>();
		for (String dm : dms) {
			try {
				String url = null;
				if (dmBatchconfig.getProtocol() != null && dm != null) {
					dm = dm.trim();
					url = new String(dmBatchconfig.getProtocol() + "://" + dm + "/wps/");
				}

				SClient sClient = new DataMinerService().getClient(dmBatchconfig.getToken(), url);
				ComputationId computationId = runSingleTest(sClient);
				String response = null;
				ComputationStatus computationStatus = monitorSingleTest(sClient, computationId);
				switch (computationStatus.getStatus()) {
				case ACCEPTED:
					break;
				case CANCELLED:
					break;
				case COMPLETE:
					response = retrieveOutputForSingleTest(sClient, computationId);
					break;
				case FAILED:
					break;
				case RUNNING:
					break;
				default:
					break;
				}

				SingleTestResult singleTestResult = new SingleTestResult(computationStatus, response);
				testResultMap.put(dm, singleTestResult);
			} catch (Exception e) {
				logger.error("Error testing: " + dm);
				ComputationStatus computationStatus = new ComputationStatus(e);
				SingleTestResult singleTestResult = new SingleTestResult(computationStatus, null);
				testResultMap.put(dm, singleTestResult);
			}
		}

		result = new BatchTestResult(testResultMap);
		return result;
	}

	private ComputationId runSingleTest(SClient sClient) throws ServiceException {
		try {
			List<OperatorsClassification> operatorsClassifications = sClient.getOperatorsClassifications();
			logger.debug("OperatorsClassifications: " + operatorsClassifications);

			Operator operator = sClient.getOperatorById(dmTest.getId());

			if (operator == null) {
				logger.error("Operator not found");
				throw new ServiceException("Operator: " + dmTest.getId() + " not found");
			} else {
				logger.debug("Operator Name: " + operator.getName() + " (" + operator.getId() + ")");
				logger.debug("Operator: " + operator);
				List<Parameter> parameters = sClient.getInputParameters(operator);
				logger.debug("Parameters: " + parameters);
				for (Parameter parameter : parameters) {
					logger.debug(
							"Parameter:[Name=" + parameter.getName() + ", Typology=" + parameter.getTypology() + "]");
				}

				dmTest.createRequest(operator);
				logger.debug("Start Computation");
				ComputationId computationId = sClient.startComputation(operator);
				logger.debug("Started ComputationId: " + computationId);
				return computationId;
			}
		} catch (ServiceException e) {
			throw e;
		} catch (Throwable e) {
			throw new ServiceException(e.getLocalizedMessage(), e);
		}

	}

	private ComputationStatus monitorSingleTest(SClient sClient, ComputationId computationId) throws ServiceException {
		try {
			logger.debug("Requesting operation progress");
			ComputationStatus computationStatus = null;
			Status status = null;

			while (status == null || status.compareTo(Status.ACCEPTED) == 0 || status.compareTo(Status.RUNNING) == 0) {
				try {
					computationStatus = sClient.getComputationStatus(computationId);
				} catch (Exception e) {
					logger.error("Error retrieving computation Status:" + e.getLocalizedMessage(), e);
					throw new ServiceException("Error retrieving computation Status:" + e.getLocalizedMessage(), e);

				}
				logger.debug("ComputationStatus: " + computationStatus);
				if (computationStatus == null) {
					logger.error("ComputationStatus is null");
					throw new ServiceException("Error retrieving computation Status: ComputationStatus is null");
				}

				status = computationStatus.getStatus();
				if (status == null) {
					logger.error("Status is null");
					throw new ServiceException("Error retrieving computation Status: Status is null");
				} else {
					switch (status) {
					case CANCELLED:
					case COMPLETE:
					case FAILED:
						break;
					case ACCEPTED:
					case RUNNING:
					default:
						try {
							Thread.sleep(Constants.TEST_MONITOR_PERIODMILLIS);
						} catch (InterruptedException e) {

						}
						break;
					}
				}

			}
			return computationStatus;
		} catch (ServiceException e) {
			logger.error(e.getLocalizedMessage(), e);
			throw e;
		} catch (Throwable e) {
			logger.error(e.getLocalizedMessage(), e);
			throw new ServiceException(e.getLocalizedMessage(), e);
		}
	}

	private String retrieveOutputForSingleTest(SClient sClient, ComputationId computationId) throws ServiceException {
		try {
			OutputData outputData = sClient.getOutputDataByComputationId(computationId);

			if (dmTest.isValidResult(outputData)) {
				return dmTest.getResult(outputData);
			} else {
				throw new ServiceException("Invalid output data for test: " + dmTest.getId());
			}

		} catch (ServiceException e) {
			logger.error(e.getLocalizedMessage());
			throw e;
		} catch (Throwable e) {
			logger.error(e.getLocalizedMessage(), e);
			throw new ServiceException(e.getLocalizedMessage(), e);
		}
	}

}
