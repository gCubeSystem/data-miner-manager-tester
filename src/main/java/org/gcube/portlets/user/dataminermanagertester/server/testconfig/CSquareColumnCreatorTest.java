package org.gcube.portlets.user.dataminermanagertester.server.testconfig;

import java.util.ArrayList;
import java.util.List;

import org.gcube.data.analysis.dataminermanagercl.shared.data.OutputData;
import org.gcube.data.analysis.dataminermanagercl.shared.data.output.MapResource;
import org.gcube.data.analysis.dataminermanagercl.shared.data.output.Resource;
import org.gcube.data.analysis.dataminermanagercl.shared.parameters.ObjectParameter;
import org.gcube.data.analysis.dataminermanagercl.shared.parameters.Parameter;
import org.gcube.data.analysis.dataminermanagercl.shared.parameters.TabularParameter;
import org.gcube.data.analysis.dataminermanagercl.shared.process.Operator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class CSquareColumnCreatorTest implements DMTest {
	private static Logger logger = LoggerFactory.getLogger(CSquareColumnCreatorTest.class);
	private static final String id = "org.gcube.dataanalysis.wps.statisticalmanager.synchserver.mappedclasses.transducerers.CSQUARE_COLUMN_CREATOR";

	@Override
	public String getId() {
		return id;
	}

	@Override
	public void createRequest(Operator operator) {
		logger.debug("Create Request");

		TabularParameter occurencePointsTable = new TabularParameter();
		occurencePointsTable.setName("InputTable");
		occurencePointsTable
				.setValue("https://data.d4science.org/shub/E_SDBhbEovMTFMWGx0TG9qYXVOS0ExanJoWjkzbHBmZWpNTWpKOVRqNnQvRGk3ZFZTb09HUGdUbDRkanVoRHdlSA==");

		ObjectParameter longitude = new ObjectParameter();
		longitude.setName("Longitude_Column");
		longitude.setValue("Longitude");
		
		ObjectParameter latitude = new ObjectParameter();
		latitude.setName("Latitude_Column");
		latitude.setValue("Latitude");
		
		ObjectParameter cSquareResolution = new ObjectParameter();
		cSquareResolution.setName("CSquare_Resolution");
		cSquareResolution.setValue("0.1");
		
		ObjectParameter outputTableName = new ObjectParameter();
		outputTableName.setName("OutputTableName");
		outputTableName.setValue("wps_csquare_column");
		

		List<Parameter> parameters = new ArrayList<>();
		parameters.add(occurencePointsTable);
		parameters.add(longitude);
		parameters.add(latitude);
		parameters.add(cSquareResolution);
		parameters.add(outputTableName);
		
		
		logger.debug("Parameters set: " + parameters);
		operator.setOperatorParameters(parameters);

	}

	@Override
	public String getResult(OutputData outputData) {
		StringBuilder result = new StringBuilder();
		logger.debug("Output: " + outputData);
		Resource resource = outputData.getResource();
		if (resource.isMap()) {
			MapResource mapResource = (MapResource) resource;
			for (String key : mapResource.getMap().keySet()) {
				logger.debug("Entry: " + key + " = " + mapResource.getMap().get(key));
				result.append("Entry: " + key + " = " + mapResource.getMap().get(key));
			}

		} else {

		}
		return result.toString();
	}

	@Override
	public boolean isValidResult(OutputData outputData) {
		boolean valid;
		logger.debug("Output: " + outputData);
		Resource resource = outputData.getResource();
		if (resource.isMap()) {
			MapResource mapResource = (MapResource) resource;
			for (String key : mapResource.getMap().keySet()) {
				logger.debug("Entry: " + key + " = " + mapResource.getMap().get(key));
			}
			valid = true;

		} else {
			valid = false;
		}
		return valid;

	}

}
