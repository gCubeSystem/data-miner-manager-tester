package org.gcube.portlets.user.dataminermanagertester.client.application.testconfig;

import com.gwtplatform.mvp.client.gin.AbstractPresenterModule;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
public class TestConfModule extends AbstractPresenterModule {
    @Override
    protected void configure() {
        bindPresenter(TestConfPresenter.class, TestConfPresenter.PresenterView.class, 
        		TestConfView.class, TestConfPresenter.PresenterProxy.class);
    }
}