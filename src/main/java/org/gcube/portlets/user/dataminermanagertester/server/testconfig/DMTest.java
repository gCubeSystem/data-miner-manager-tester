package org.gcube.portlets.user.dataminermanagertester.server.testconfig;

import org.gcube.data.analysis.dataminermanagercl.shared.data.OutputData;
import org.gcube.data.analysis.dataminermanagercl.shared.process.Operator;

/**
 * 
 * @author Giancarlo Panichi 
 *
 *
 */
public interface DMTest {
	
	public String getId();
	
	public void createRequest(Operator operator);

	public boolean isValidResult(OutputData outputData);
	
	public String getResult(OutputData outputData);
	
}
