package org.gcube.portlets.user.dataminermanagertester.server.testbuild;

import org.gcube.portlets.user.dataminermanagertester.server.testconfig.BionymLocalTest;
import org.gcube.portlets.user.dataminermanagertester.server.testconfig.CMSY2Test;
import org.gcube.portlets.user.dataminermanagertester.server.testconfig.CSquareColumnCreatorTest;
import org.gcube.portlets.user.dataminermanagertester.server.testconfig.DBScanTest;
import org.gcube.portlets.user.dataminermanagertester.server.testconfig.DMTest;
import org.gcube.portlets.user.dataminermanagertester.server.testconfig.FeedForwardAnnTest;
import org.gcube.portlets.user.dataminermanagertester.server.testconfig.GenericChartsTest;
import org.gcube.portlets.user.dataminermanagertester.server.testconfig.ListDBNameTest;
import org.gcube.portlets.user.dataminermanagertester.server.testconfig.OpenMeshRecostructorGPUTest;
import org.gcube.portlets.user.dataminermanagertester.server.testconfig.PolygonsToMapTest;
import org.gcube.portlets.user.dataminermanagertester.server.testconfig.RasterDataPublisherTest;
import org.gcube.portlets.user.dataminermanagertester.server.testconfig.WebAppPublisherTest;
import org.gcube.portlets.user.dataminermanagertester.server.testconfig.XYExtractorTest;
import org.gcube.portlets.user.dataminermanagertester.shared.config.TestType;
import org.gcube.portlets.user.dataminermanagertester.shared.exception.ServiceException;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class TestBuilder {

	public DMTest build(TestType testType) throws ServiceException {
		if (testType != null) {
			switch (testType) {
			case FeedForwardAnn:
				return new FeedForwardAnnTest();
			case BionymLocal:
				return new BionymLocalTest();
			case CMSY2:
				return new CMSY2Test();
			case RasterDataPublisher:
				return new RasterDataPublisherTest();
			case WebAppPublisher:
				return new WebAppPublisherTest();
			case XYExtractor:
				return new XYExtractorTest();
			case CSquareColumnCreator:
				return new CSquareColumnCreatorTest();
			case DBScan:
				return new DBScanTest();
			case ListDBName:
				return new ListDBNameTest();
			case OpenMeshRecostructorGPU:
				return new OpenMeshRecostructorGPUTest();
			case GenericCharts:
				return new GenericChartsTest();
			case PolygonsToMap:
				return new PolygonsToMapTest();
			default:
				throw new ServiceException("Test not found");
			}
		} else {
			throw new ServiceException("Test not found");
		}
	}
}
