package org.gcube.portlets.user.dataminermanagertester.client.application.testconfig;


import org.gcube.portlets.user.dataminermanagertester.shared.config.DMConfig;

import com.gwtplatform.mvp.client.UiHandlers;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
interface TestConfUiHandlers extends UiHandlers {

	void executeTest(DMConfig dmConfig);
}
