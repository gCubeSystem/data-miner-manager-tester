package org.gcube.portlets.user.dataminermanagertester.client.application.custom;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class SelectableTextCell extends AbstractCell<String> {

	@Override
	public void render(Context context, String value, SafeHtmlBuilder sb) {
		if (value != null) {
			sb.appendHtmlConstant("<div unselectable='false' >" + value + "</div>");
		}
	}

}
