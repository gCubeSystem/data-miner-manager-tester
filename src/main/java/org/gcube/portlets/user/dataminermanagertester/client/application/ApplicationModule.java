package org.gcube.portlets.user.dataminermanagertester.client.application;


import org.gcube.portlets.user.dataminermanagertester.client.application.diff.OperatorsDiffModule;
import org.gcube.portlets.user.dataminermanagertester.client.application.help.HelpModule;
import org.gcube.portlets.user.dataminermanagertester.client.application.home.HomeModule;
import org.gcube.portlets.user.dataminermanagertester.client.application.menu.MenuModule;
import org.gcube.portlets.user.dataminermanagertester.client.application.testbatchconfig.TestBatchConfModule;
import org.gcube.portlets.user.dataminermanagertester.client.application.testconfig.TestConfModule;

import com.gwtplatform.mvp.client.gin.AbstractPresenterModule;



/**
 * 
 * @author Giancarlo Panichi
 *
 */
public class ApplicationModule extends AbstractPresenterModule {
	@Override
	protected void configure() {
		install(new MenuModule());
		install(new HomeModule());
		install(new HelpModule());
		install(new TestConfModule());
		install(new TestBatchConfModule());
		install(new OperatorsDiffModule());
		
		bindPresenter(ApplicationPresenter.class, ApplicationPresenter.PresenterView.class, ApplicationView.class,
				ApplicationPresenter.PresenterProxy.class);
	}
}
