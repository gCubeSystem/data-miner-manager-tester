package org.gcube.portlets.user.dataminermanagertester.client.application.diff;

import com.gwtplatform.mvp.client.gin.AbstractPresenterModule;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
public class OperatorsDiffModule extends AbstractPresenterModule {
    @Override
    protected void configure() {
        bindPresenter(OperatorsDiffPresenter.class, OperatorsDiffPresenter.PresenterView.class, 
        		OperatorsDiffView.class, OperatorsDiffPresenter.PresenterProxy.class);
    }
}