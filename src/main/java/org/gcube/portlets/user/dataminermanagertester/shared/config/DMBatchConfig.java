package org.gcube.portlets.user.dataminermanagertester.shared.config;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class DMBatchConfig implements Serializable {

	private static final long serialVersionUID = 4251753074961060428L;
	private ArrayList<String> dms;
	private String token;
	private String protocol;
	private String testType;

	public DMBatchConfig() {
		super();
	}

	public DMBatchConfig(ArrayList<String> dms, String token, String protocol, String testType) {
		super();
		this.dms = dms;
		this.token = token;
		this.protocol = protocol;
		this.testType = testType;
	}

	public ArrayList<String> getDms() {
		return dms;
	}

	public void setDms(ArrayList<String> dms) {
		this.dms = dms;
	}

	public ArrayList<String> getDmsUrls() {
		ArrayList<String> dmsUrls = new ArrayList<>();
		if (protocol != null) {
			if (dms != null && !dms.isEmpty()) {
				for (int i = 0; i < dms.size(); i++) {
					String dmName = dms.get(i);
					String url = new String(protocol + "://" + dmName + "/wps/");
					dmsUrls.add(url);
				}
			}
		}
		return dmsUrls;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getTestType() {
		return testType;
	}

	public void setTestType(String testType) {
		this.testType = testType;
	}

	@Override
	public String toString() {
		return "DMBatchConfig [dms=" + dms + ", token=" + token + ", protocol=" + protocol + ", testType=" + testType
				+ "]";
	}

}
