package org.gcube.portlets.user.dataminermanagertester.client.application.diff;


import org.gcube.portlets.user.dataminermanagertester.shared.config.DMDiffConfig;

import com.gwtplatform.mvp.client.UiHandlers;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
interface OperatorsDiffUiHandlers extends UiHandlers {

	void executeDiff(DMDiffConfig operatorsDiffConfig);
}
