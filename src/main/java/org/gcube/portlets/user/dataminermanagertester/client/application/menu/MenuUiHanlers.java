package org.gcube.portlets.user.dataminermanagertester.client.application.menu;


import com.gwtplatform.mvp.client.UiHandlers;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
interface MenuUiHandlers extends UiHandlers {

	void setContentPush();
}
