package org.gcube.portlets.user.dataminermanagertester.server.testconfig;

import java.util.ArrayList;
import java.util.List;

import org.gcube.data.analysis.dataminermanagercl.shared.data.OutputData;
import org.gcube.data.analysis.dataminermanagercl.shared.data.output.MapResource;
import org.gcube.data.analysis.dataminermanagercl.shared.data.output.Resource;
import org.gcube.data.analysis.dataminermanagercl.shared.parameters.FileParameter;
import org.gcube.data.analysis.dataminermanagercl.shared.parameters.ObjectParameter;
import org.gcube.data.analysis.dataminermanagercl.shared.parameters.Parameter;
import org.gcube.data.analysis.dataminermanagercl.shared.process.Operator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class RasterDataPublisherTest implements DMTest {
	private static Logger logger = LoggerFactory.getLogger(RasterDataPublisherTest.class);
	private static final String id = "org.gcube.dataanalysis.wps.statisticalmanager.synchserver.mappedclasses.transducerers.RASTER_DATA_PUBLISHER";

	
	@Override
	public String getId() {
		return id;
	}

	
	
	@Override
	public void createRequest(Operator operator) {
		logger.debug("Create Request");

		ObjectParameter publicationLevel = new ObjectParameter();
		publicationLevel.setName("PublicationLevel");
		publicationLevel.setValue("PUBLIC");
		
		ObjectParameter datasetAbstract = new ObjectParameter();
		datasetAbstract.setName("DatasetAbstract");
		datasetAbstract.setValue("Abstract");

		ObjectParameter datasetTitle = new ObjectParameter();
		datasetTitle.setName("DatasetTitle");
		datasetTitle.setValue("Generic Raster Layer Test3");
		
		FileParameter rasterFile=new FileParameter();
		rasterFile.setName("RasterFile");
		rasterFile.setValue("https://data.d4science.org/QTVNbXp5cmI0MG52TTE0K2paNzhXZWlCTHhweU8rUCtHbWJQNStIS0N6Yz0");
		
		ObjectParameter innerLayerName = new ObjectParameter();
		innerLayerName.setName("InnerLayerName");
		innerLayerName.setValue("analyzed_field");
		
		ObjectParameter fileNameOnInfra = new ObjectParameter();
		fileNameOnInfra.setName("FileNameOnInfra");
		fileNameOnInfra.setValue("raster-1465493226242.nc");
		
		ObjectParameter topics = new ObjectParameter();
		topics.setName("Topics");
		topics.setValue("analyzed_field");
		
		ObjectParameter spatialResolution = new ObjectParameter();
		spatialResolution.setName("SpatialResolution");
		spatialResolution.setValue("-1");
		
		
		List<Parameter> parameters = new ArrayList<>();
		
		parameters.add(publicationLevel);
		parameters.add(datasetAbstract);
		parameters.add(datasetTitle);
		parameters.add(rasterFile);
		parameters.add(innerLayerName);
		parameters.add(fileNameOnInfra);
		parameters.add(topics);
		parameters.add(spatialResolution);
		
		logger.debug("Parameters set: " + parameters);
		operator.setOperatorParameters(parameters);

	}

	@Override
	public String getResult(OutputData outputData) {
		StringBuilder result=new StringBuilder();
		logger.debug("Output: " + outputData);
		Resource resource = outputData.getResource();
		if (resource.isMap()) {
			MapResource mapResource = (MapResource) resource;
			for (String key : mapResource.getMap().keySet()) {
				logger.debug("Entry: " + key + " = "
						+ mapResource.getMap().get(key));
				result.append("Entry: " + key + " = "
						+ mapResource.getMap().get(key));
			}
			
		} else {
			
		}
		return result.toString();
	}

	@Override
	public boolean isValidResult(OutputData outputData) {
		boolean valid;
		logger.debug("Output: " + outputData);
		Resource resource = outputData.getResource();
		if (resource.isMap()) {
			MapResource mapResource = (MapResource) resource;
			for (String key : mapResource.getMap().keySet()) {
				logger.debug("Entry: " + key + " = "
						+ mapResource.getMap().get(key));
			}
			valid=true;
			
		} else {
			valid=false;
		}
		return valid;
	
	}
	
}
