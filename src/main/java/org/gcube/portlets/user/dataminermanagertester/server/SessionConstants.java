package org.gcube.portlets.user.dataminermanagertester.server;

public class SessionConstants {
	
	//Context
	public static final String DATAMINERTESTER_MONITOR_TIME_OUT_PERIODMILLIS = "DATAMINERTESTER_MONITOR_TIME_OUT_PERIODMILLIS";

	//Session
	public static final String TASK_WRAPPER_MAP = "TASK_WRAPPER_MAP";
	public static final String TASK_REQUEST_QUEUE = "TASK_REQUEST_QUEUE";
	

}