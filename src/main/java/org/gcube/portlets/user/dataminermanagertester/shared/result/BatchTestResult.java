package org.gcube.portlets.user.dataminermanagertester.shared.result;

import java.io.Serializable;
import java.util.LinkedHashMap;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class BatchTestResult implements Serializable {

	private static final long serialVersionUID = -8282297686097887134L;

	private LinkedHashMap<String, SingleTestResult> testResultMap;

	public BatchTestResult() {
		super();
	}

	public BatchTestResult(LinkedHashMap<String, SingleTestResult> testResultMap) {
		super();
		this.testResultMap = testResultMap;
	}

	public LinkedHashMap<String, SingleTestResult> getTestResultMap() {
		return testResultMap;
	}

	public void setTestResultMap(LinkedHashMap<String, SingleTestResult> testResultMap) {
		this.testResultMap = testResultMap;
	}

	@Override
	public String toString() {
		return "BatchTestResult [testResultMap=" + testResultMap + "]";
	}

}
