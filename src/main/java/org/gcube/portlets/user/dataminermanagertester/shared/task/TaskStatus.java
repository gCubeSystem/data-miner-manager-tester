package org.gcube.portlets.user.dataminermanagertester.shared.task;


/**
 * 
  * @author Giancarlo Panichi
 *
 *
 */
public enum TaskStatus {
	STARTED,
	RUNNING,
	COMPLETED,
	ERROR;
}