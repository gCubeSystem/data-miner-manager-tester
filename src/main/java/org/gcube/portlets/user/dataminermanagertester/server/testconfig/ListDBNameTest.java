package org.gcube.portlets.user.dataminermanagertester.server.testconfig;

import java.util.ArrayList;
import java.util.List;

import org.gcube.data.analysis.dataminermanagercl.shared.data.OutputData;
import org.gcube.data.analysis.dataminermanagercl.shared.data.output.MapResource;
import org.gcube.data.analysis.dataminermanagercl.shared.data.output.Resource;
import org.gcube.data.analysis.dataminermanagercl.shared.parameters.ObjectParameter;
import org.gcube.data.analysis.dataminermanagercl.shared.parameters.Parameter;
import org.gcube.data.analysis.dataminermanagercl.shared.process.Operator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class ListDBNameTest implements DMTest {
	private static Logger logger = LoggerFactory.getLogger(ListDBNameTest.class);
	private static final String id = "org.gcube.dataanalysis.wps.statisticalmanager.synchserver.mappedclasses.transducerers.LISTDBNAMES";

	
	@Override
	public String getId() {
		return id;
	}

	@Override
	public void createRequest(Operator operator) {
		logger.debug("Create Request");

		ObjectParameter maxNumber = new ObjectParameter();
		maxNumber.setName("MaxNumber");
		maxNumber.setValue("-1");

		List<Parameter> parameters = new ArrayList<>();
		
		parameters.add(maxNumber);
		
		logger.debug("Parameters set: " + parameters);
		operator.setOperatorParameters(parameters);

	}

	@Override
	public String getResult(OutputData outputData) {
		StringBuilder result=new StringBuilder();
		logger.debug("Output: " + outputData);
		Resource resource = outputData.getResource();
		if (resource.isMap()) {
			MapResource mapResource = (MapResource) resource;
			for (String key : mapResource.getMap().keySet()) {
				logger.debug("Entry: " + key + " = "
						+ mapResource.getMap().get(key));
				result.append("Entry: " + key + " = "
						+ mapResource.getMap().get(key));
			}
			
		} else {
			
		}
		return result.toString();
	}

	@Override
	public boolean isValidResult(OutputData outputData) {
		boolean valid;
		logger.debug("Output: " + outputData);
		Resource resource = outputData.getResource();
		if (resource.isMap()) {
			MapResource mapResource = (MapResource) resource;
			for (String key : mapResource.getMap().keySet()) {
				logger.debug("Entry: " + key + " = "
						+ mapResource.getMap().get(key));
			}
			valid=true;
			
		} else {
			valid=false;
		}
		return valid;
	
	}
	
}
