package org.gcube.portlets.user.dataminermanagertester.shared.config;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public enum TestType {
	Capabilities("Capabilities"), DBScan("DBScan"), BionymLocal("Bionym Local"), CMSY2("CMSY 2"), CSquareColumnCreator(
			"CSquare Column Creator"), FeedForwardAnn("Feed Forward ANN"), GenericCharts("Generic Charts"), ListDBName(
					"List DB Name"), OpenMeshRecostructorGPU("Open Mesh Recostructor GPU"), PolygonsToMap(
							"Polygons To Map"), RasterDataPublisher("Raster Data Publisher"), XYExtractor(
									"XYExtractor"), WebAppPublisher("Web App Publisher");

	private TestType(final String id) {
		this.id = id;
	}

	private final String id;

	@Override
	public String toString() {
		return id;
	}

	public String getLabel() {
		return id;
	}

	public boolean compareId(String identificator) {
		if (identificator.compareTo(id) == 0) {
			return true;
		} else {
			return false;
		}
	}

	public static TestType getTypeFromId(String id) {
		for (TestType testType : values()) {
			if (testType.id.compareToIgnoreCase(id) == 0) {
				return testType;
			}
		}
		return null;
	}

}