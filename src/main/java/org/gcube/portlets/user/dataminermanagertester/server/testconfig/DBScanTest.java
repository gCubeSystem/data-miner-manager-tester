package org.gcube.portlets.user.dataminermanagertester.server.testconfig;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.gcube.data.analysis.dataminermanagercl.shared.data.OutputData;
import org.gcube.data.analysis.dataminermanagercl.shared.data.output.FileResource;
import org.gcube.data.analysis.dataminermanagercl.shared.data.output.ImageResource;
import org.gcube.data.analysis.dataminermanagercl.shared.data.output.MapResource;
import org.gcube.data.analysis.dataminermanagercl.shared.data.output.Resource;
import org.gcube.data.analysis.dataminermanagercl.shared.data.output.TableResource;
import org.gcube.data.analysis.dataminermanagercl.shared.parameters.ColumnListParameter;
import org.gcube.data.analysis.dataminermanagercl.shared.parameters.ObjectParameter;
import org.gcube.data.analysis.dataminermanagercl.shared.parameters.Parameter;
import org.gcube.data.analysis.dataminermanagercl.shared.parameters.TabularParameter;
import org.gcube.data.analysis.dataminermanagercl.shared.process.Operator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class DBScanTest implements DMTest {
	private static Logger logger = LoggerFactory.getLogger(DBScanTest.class);
	private static final String id = "org.gcube.dataanalysis.wps.statisticalmanager.synchserver.mappedclasses.clusterers.DBSCAN";

	@Override
	public String getId() {
		return id;
	}

	@Override
	public void createRequest(Operator operator) {
		logger.debug("Create Request");

		TabularParameter occurencePointsTable = new TabularParameter();
		occurencePointsTable.setName("OccurrencePointsTable");
		occurencePointsTable
				.setValue("https://data.d4science.org/YmRKWDU4Y0RJT2hSLzcybU4zRmJoTEg2YTBMWlRZVUpHbWJQNStIS0N6Yz0");

		ColumnListParameter columnListParameter = new ColumnListParameter();
		columnListParameter.setName("FeaturesColumnNames");
		columnListParameter.setValue("depthmean|sstmnmax|salinitymin");

		ObjectParameter occurencePointsClusterLabel = new ObjectParameter();
		occurencePointsClusterLabel.setName("OccurrencePointsClusterLabel");
		occurencePointsClusterLabel.setValue("Test");

		ObjectParameter epsilon = new ObjectParameter();
		epsilon.setName("epsilon");
		epsilon.setValue("10");

		ObjectParameter minPoints = new ObjectParameter();
		minPoints.setName("min_points");
		minPoints.setValue("1");

		List<Parameter> parameters = new ArrayList<>();
		parameters.add(occurencePointsTable);
		parameters.add(columnListParameter);
		parameters.add(occurencePointsClusterLabel);
		parameters.add(epsilon);
		parameters.add(minPoints);

		logger.debug("Parameters set: " + parameters);
		operator.setOperatorParameters(parameters);

	}

	@Override
	public String getResult(OutputData outputData) {
		StringBuilder result = new StringBuilder();
		logger.debug("Output: " + outputData);
		Resource resource = outputData.getResource();
		if (resource.isMap()) {
			MapResource mapResource = (MapResource) resource;
			for (String key : mapResource.getMap().keySet()) {
				Resource res = mapResource.getMap().get(key);
				switch (res.getResourceType()) {
				case FILE:
					FileResource fileResource = (FileResource) res;
					String fileName = retrieveFileName(fileResource.getUrl());
					logger.debug("Entry: " + key + " = " + mapResource.getMap().get(key) + ", FileName=" + fileName);
					result.append("Entry: " + key + " = " + mapResource.getMap().get(key) + ", FileName=" + fileName);
					break;
				case IMAGE:
					ImageResource imageResource = (ImageResource) res;
					String imageName = retrieveFileName(imageResource.getLink());
					logger.debug("Entry: " + key + " = " + mapResource.getMap().get(key) + ", ImageName=" + imageName);
					result.append("Entry: " + key + " = " + mapResource.getMap().get(key) + ", ImageName=" + imageName);
					break;
				case MAP:
					logger.debug("Entry: " + key + " = " + mapResource.getMap().get(key));
					result.append("Entry: " + key + " = " + mapResource.getMap().get(key));

					break;
				case OBJECT:
					logger.debug("Entry: " + key + " = " + mapResource.getMap().get(key));
					result.append("Entry: " + key + " = " + mapResource.getMap().get(key));
					break;
				case TABULAR:
					TableResource tableResource = (TableResource) res;
					String tableName = retrieveFileName(tableResource.getResourceId());
					logger.debug("Entry: " + key + " = " + mapResource.getMap().get(key) + ", TableName=" + tableName);
					result.append("Entry: " + key + " = " + mapResource.getMap().get(key) + ", TableName=" + tableName);
					break;
				default:
					logger.debug("Entry: " + key + " = " + mapResource.getMap().get(key));
					result.append("Entry: " + key + " = " + mapResource.getMap().get(key));
					break;
				}

			}

		} else {

		}
		return result.toString();

	}

	@Override
	public boolean isValidResult(OutputData outputData) {
		boolean valid;
		logger.debug("Output: " + outputData);
		Resource resource = outputData.getResource();
		if (resource.isMap()) {
			MapResource mapResource = (MapResource) resource;
			for (String key : mapResource.getMap().keySet()) {
				logger.debug("Entry: " + key + " = " + mapResource.getMap().get(key));
			}
			valid = true;

		} else {
			valid = false;
		}
		return valid;

	}

	private String retrieveFileName(String url) {
		String fileName = "output";

		try {

			URL urlObj;
			urlObj = new URL(url);

			HttpURLConnection connection = (HttpURLConnection) urlObj.openConnection();
			connection.setRequestMethod("GET");
			String contentDisposition = connection.getHeaderField("Content-Disposition");
			Pattern regex = Pattern.compile("(?<=filename=\").*?(?=\")");
			Matcher regexMatcher = regex.matcher(contentDisposition);
			if (regexMatcher.find()) {
				fileName = regexMatcher.group();
			}

			if (fileName == null || fileName.isEmpty()) {
				fileName = "output";
			}

			return fileName;
		} catch (Throwable e) {
			logger.error("Error retrieving file name: " + e.getLocalizedMessage(), e);
			return fileName;
		}

	}

}
