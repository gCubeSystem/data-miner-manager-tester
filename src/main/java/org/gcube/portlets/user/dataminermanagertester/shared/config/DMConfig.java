package org.gcube.portlets.user.dataminermanagertester.shared.config;

import java.io.Serializable;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class DMConfig implements Serializable {

	private static final long serialVersionUID = 4251753074961060428L;
	private String dm;
	private String token;
	private String protocol;
	private String testType;

	public DMConfig() {
		super();
	}

	public DMConfig(String dm, String token, String protocol, String testType) {
		super();
		this.dm = dm;
		this.token = token;
		this.protocol = protocol;
		this.testType = testType;
	}

	public String getDm() {
		return dm;
	}

	public void setDm(String dm) {
		this.dm = dm;
	}

	public String getDmUrl() {
		String url = null;
		if (protocol != null && dm != null) {
			url = new String(protocol + "://" + dm + "/wps/");
		}
		return url;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getTestType() {
		return testType;
	}

	public void setTestType(String testType) {
		this.testType = testType;
	}

	@Override
	public String toString() {
		return "DMConfig [dm=" + dm + ", token=" + token + ", protocol=" + protocol + ", testType=" + testType + "]";
	}

}
