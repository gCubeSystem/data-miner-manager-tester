package org.gcube.portlets.user.dataminermanagertester.shared.config;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public enum ProtocolType {
	HTTPS("https"), HTTP("http");

	private ProtocolType(final String id) {
		this.id = id;
	}

	private final String id;

	@Override
	public String toString() {
		return id;
	}

	public String getLabel() {
		return id;
	}

	public boolean compareId(String identificator) {
		if (identificator.compareTo(id) == 0) {
			return true;
		} else {
			return false;
		}
	}

	public static ProtocolType getTypeFromId(String id) {
		for (ProtocolType testType : values()) {
			if (testType.id.compareToIgnoreCase(id) == 0) {
				return testType;
			}
		}
		return null;
	}

}