package org.gcube.portlets.user.dataminermanagertester.client.place;


/**
 * 
 * @author Giancarlo Panichi
 *
 */
public class TokenParameters {
    public static final String ID = "id";
    
    public static String getId() {
		return ID;
	}
}