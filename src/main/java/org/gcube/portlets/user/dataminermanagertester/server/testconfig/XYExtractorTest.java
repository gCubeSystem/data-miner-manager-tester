package org.gcube.portlets.user.dataminermanagertester.server.testconfig;

import java.util.ArrayList;
import java.util.List;

import org.gcube.data.analysis.dataminermanagercl.shared.data.OutputData;
import org.gcube.data.analysis.dataminermanagercl.shared.data.output.MapResource;
import org.gcube.data.analysis.dataminermanagercl.shared.data.output.Resource;
import org.gcube.data.analysis.dataminermanagercl.shared.parameters.ObjectParameter;
import org.gcube.data.analysis.dataminermanagercl.shared.parameters.Parameter;
import org.gcube.data.analysis.dataminermanagercl.shared.process.Operator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class XYExtractorTest implements DMTest {
	private static Logger logger = LoggerFactory.getLogger(XYExtractorTest.class);
	private static final String id = "org.gcube.dataanalysis.wps.statisticalmanager.synchserver.mappedclasses.transducerers.XYEXTRACTOR";

	
	@Override
	public String getId() {
		return id;
	}
			
	
	@Override
	public void createRequest(Operator operator) {
		logger.debug("Create Request");

		ObjectParameter outputTableLabel = new ObjectParameter();
		outputTableLabel.setName("OutputTableLabel");
		outputTableLabel.setValue("wps_xy_extractor");
		
		ObjectParameter layer = new ObjectParameter();
		layer.setName("Layer");
		layer.setValue("3fb7fd88-33d4-492d-b241-4e61299c44bb");

		ObjectParameter yResolution = new ObjectParameter();
		yResolution.setName("YResolution");
		yResolution.setValue("0.5");
		
		ObjectParameter xResolution = new ObjectParameter();
		xResolution.setName("XResolution");
		xResolution.setValue("0.5");
		
		ObjectParameter bBox_LowerLeftLong = new ObjectParameter();
		bBox_LowerLeftLong.setName("BBox_LowerLeftLong");
		bBox_LowerLeftLong.setValue("-50");
		
		ObjectParameter bBox_UpperRightLat = new ObjectParameter();
		bBox_UpperRightLat.setName("BBox_UpperRightLat");
		bBox_UpperRightLat.setValue("60");
				
		ObjectParameter bBox_LowerLeftLat = new ObjectParameter();
		bBox_LowerLeftLat.setName("BBox_LowerLeftLat");
		bBox_LowerLeftLat.setValue("-60");
		
		ObjectParameter bBox_UpperRightLong = new ObjectParameter();
		bBox_UpperRightLong.setName("BBox_UpperRightLong");
		bBox_UpperRightLong.setValue("50");
		
		ObjectParameter z = new ObjectParameter();
		z.setName("Z");
		z.setValue("0");
		
		ObjectParameter timeIndex = new ObjectParameter();
		timeIndex.setName("TimeIndex");
		timeIndex.setValue("0");
		
		
		List<Parameter> parameters = new ArrayList<>();
		
		parameters.add(outputTableLabel);
		parameters.add(layer);
		parameters.add(yResolution);
		parameters.add(xResolution);
		parameters.add(bBox_LowerLeftLong);
		parameters.add(bBox_UpperRightLat);
		parameters.add(bBox_LowerLeftLat);
		parameters.add(bBox_UpperRightLong);
		parameters.add(z);
		parameters.add(timeIndex);
		
		
		logger.debug("Parameters set: " + parameters);
		operator.setOperatorParameters(parameters);

	}

	@Override
	public String getResult(OutputData outputData) {
		StringBuilder result=new StringBuilder();
		logger.debug("Output: " + outputData);
		Resource resource = outputData.getResource();
		if (resource.isMap()) {
			MapResource mapResource = (MapResource) resource;
			for (String key : mapResource.getMap().keySet()) {
				logger.debug("Entry: " + key + " = "
						+ mapResource.getMap().get(key));
				result.append("Entry: " + key + " = "
						+ mapResource.getMap().get(key));
			}
			
		} else {
			
		}
		return result.toString();
	}

	@Override
	public boolean isValidResult(OutputData outputData) {
		boolean valid;
		logger.debug("Output: " + outputData);
		Resource resource = outputData.getResource();
		if (resource.isMap()) {
			MapResource mapResource = (MapResource) resource;
			for (String key : mapResource.getMap().keySet()) {
				logger.debug("Entry: " + key + " = "
						+ mapResource.getMap().get(key));
			}
			valid=true;
			
		} else {
			valid=false;
		}
		return valid;
	
	}
	
}
