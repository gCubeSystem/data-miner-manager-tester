package org.gcube.portlets.user.dataminermanagertester;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.gcube.data.analysis.dataminermanagercl.server.DataMinerService;
import org.gcube.data.analysis.dataminermanagercl.server.dmservice.SClient;
import org.gcube.data.analysis.dataminermanagercl.shared.process.Operator;
import org.gcube.data.analysis.dataminermanagercl.shared.process.OperatorsClassification;
import org.gcube.portlets.user.dataminermanagertester.shared.Constants;
import org.gcube.portlets.user.dataminermanagertester.shared.exception.ServiceException;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestDataMinerAlgorithmsList {
	private String token1 = "";
	private String url1 = "";

	private String token2 = "";
	private String url2 = "";

	private static Logger logger = LoggerFactory.getLogger(TestDataMinerAlgorithmsList.class);

	/**
	 * 
	 * List of algorithms in url1
	 * 
	 */
	@Test
	public void operatorsList() {

		try {
			if (Constants.TEST_ENABLE) {

				logger.debug("Test Operators List");
				SClient sClient1 = new DataMinerService().getClient(token1, url1);
				List<OperatorsClassification> operatorsClassifications = sClient1.getOperatorsClassifications();
				logger.debug("OperatorsClassifications: " + operatorsClassifications);
				StringBuilder result = new StringBuilder();

				result.append("Success\n");

				for (OperatorsClassification opClass : operatorsClassifications) {
					for (Operator op : opClass.getOperators()) {
						result.append("Operator[Name=" + op.getName() + "]");
					}
				}

				logger.debug("Result: " + result.toString());

				assertTrue("Succes ", true);
			} else {
				assertTrue("Test Disable", true);

			}

		} catch (ServiceException e) {
			logger.error(e.getLocalizedMessage());

		} catch (Throwable e) {
			logger.error("An error occurred getting the Capabilities: " + e.getLocalizedMessage(), e);
			e.printStackTrace();

		}
	}

	/**
	 * List of algorithms in url1 and not in url2
	 * 
	 */
	@Test
	public void operatorDiff() {

		try {

			if (Constants.TEST_ENABLE) {

				logger.debug("Test Operators Diff");

				SClient sClient1 = new DataMinerService().getClient(token1, url1);
				List<OperatorsClassification> operatorsClassifications1 = sClient1.getOperatorsClassifications();
				logger.debug("OperatorsClassifications1: " + operatorsClassifications1);

				SClient sClient2 = new DataMinerService().getClient(token2, url2);
				List<OperatorsClassification> operatorsClassifications2 = sClient2.getOperatorsClassifications();
				logger.debug("OperatorsClassifications2: " + operatorsClassifications2);

				logger.debug("----------------------------------");
				logger.debug("Operators in: " + url1);
				logger.debug("and not in: " + url2);
				for (OperatorsClassification opClass1 : operatorsClassifications1) {
					for (Operator op1 : opClass1.getOperators()) {
						boolean op1Found = false;
						for (OperatorsClassification opClass2 : operatorsClassifications2) {
							for (Operator op2 : opClass2.getOperators()) {
								if (op1.getId().compareTo(op2.getId()) == 0) {
									op1Found = true;
									break;
								}
							}
							if (op1Found) {
								break;
							}

						}
						if (!op1Found) {
							logger.debug("Op: " + op1.toString());
						}
					}

				}

				logger.debug("----------------------------------");
				logger.debug("Operators in: " + url2);
				logger.debug("and not in: " + url1);
				for (OperatorsClassification opClass2 : operatorsClassifications2) {
					for (Operator op2 : opClass2.getOperators()) {
						boolean op2Found = false;
						for (OperatorsClassification opClass1 : operatorsClassifications1) {
							for (Operator op1 : opClass1.getOperators()) {
								if (op2.getId().compareTo(op1.getId()) == 0) {
									op2Found = true;
									break;
								}
							}
							if (op2Found) {
								break;
							}

						}
						if (!op2Found) {
							logger.debug("Op: " + op2.toString());
						}
					}

				}

				assertTrue("Succes ", true);
			} else {
				assertTrue("Test Disable", true);

			}
		} catch (ServiceException e) {
			logger.error(e.getLocalizedMessage());

		} catch (Throwable e) {
			logger.error("An error occurred getting operators diff: " + e.getLocalizedMessage(), e);
			e.printStackTrace();

		}
	}

}
